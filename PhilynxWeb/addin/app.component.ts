﻿import { Component, OnInit } from '@angular/core';
import { I18nService } from './common/i18n/i18n.service';
import { lang } from './common/i18n/translations/lang';

@Component({
  moduleId: module.id,
  selector: 'addin',
  templateUrl: './app.component.html',
})

export class AppComponent implements OnInit {
  title: string = 'Philynx';

  constructor(private _i18n: I18nService) {
    this._i18n.init(lang);

  }

  ngOnInit() {
  }
}
