import { NgModule }       from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { DropdownModule } from 'ng2-dropdown';
import { AppComponent } from './app.component';
import { OfficeUiModule } from './office-ui/office-ui.module';
import { DemoModule } from './demo/demo.module';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome'

@NgModule({
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    OfficeUiModule,
    DemoModule,
    DropdownModule,
    HttpModule,
    Angular2FontawesomeModule
  ],
  declarations: [
    AppComponent
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}