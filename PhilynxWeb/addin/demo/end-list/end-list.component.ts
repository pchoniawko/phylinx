import { Component, OnInit } from '@angular/core';
import { EndListService } from './end-list.service';


@Component({
    moduleId: module.id,
    selector: 'end-list',
    templateUrl: './end-list.component.html',
    styleUrls: ['./end-list.component.css']
})
export class EndListComponent implements OnInit {
    constructor(private _endListService: EndListService) { }

    ngOnInit() { }

    addListOfAttachments():void{
        this._endListService.addListOfAttachmentsToContentController();
      }

    updateListOfAttachments(){
        this._endListService.updateListOfAttachments();
    }

}
