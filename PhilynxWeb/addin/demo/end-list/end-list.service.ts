import {Injectable} from '@angular/core';
import {Attachment} from '../../common/models/attachment';
import {ContainerService} from '../../common/services/container.service';
import {FormattingSettingsService} from '../formatting-settings/formatting-settings.service';
import {FormattingListSettingsService} from '../formatting-list-settings/formatting-list-settings.service';
import {BlobService} from '../../blob/blob.service';
import {createHtml} from '../../common/createHtml';

@Injectable()
export class EndListService {

    constructor(private _containerService: ContainerService,
                private _formattingSettingService: FormattingSettingsService,
                private _formattingListSettingsService: FormattingListSettingsService,
                private _blobService: BlobService) {
    }

    addListOfAttachmentsAtTheEnd(): void {
        Word.run((context) => {
            const body = context.document.body;
            const HTMLList = this.getHtmlListOfAttachments();

            body.insertParagraph('', Word.InsertLocation.end);
            body.insertHtml(HTMLList, Word.InsertLocation.end);
            return context.sync().then(() => {
            });
        }).catch((error) => {
            console.log('Error: ' + JSON.stringify(error));
            if (error instanceof OfficeExtension.Error) {
                console.log('Debug info: ' + JSON.stringify(error.debugInfo));
            }
        });
    }

    addListOfAttachmentsSelected(): void {
        Word.run((context) => {
            const range = context.document.getSelection();
            const HTMLList = this.getHtmlListOfAttachments();
            console.log("HTMLList", HTMLList);
            range.insertHtml(HTMLList, Word.InsertLocation.after);
            return context.sync().then(() => {
            });
        }).catch((error) => {
            console.log('Error: ' + JSON.stringify(error));
            if (error instanceof OfficeExtension.Error) {
                console.log('Debug info: ' + JSON.stringify(error.debugInfo));
            }
        });
    }

    addListOfAttachmentsToContentController(): void {
        Word.run((context) => {
            // let listSetting = this._formattingListSettingsService.getAttachmentsListSettings();
            // // if(listSetting==='END'){
            //     const body = context.document.body;
            //     body.select('End');
            // // }
            const range = context.document.getSelection();
            const myContentControl = range.insertContentControl();
            const HTMLList = this.getHtmlListOfAttachments();

            myContentControl.tag = 'listOfAttachments';
            myContentControl.insertHtml(HTMLList, 'replace');

            this._blobService.updateServerNames().subscribe(
                (data: any) => console.log(data),
                (err: any) => console.error(err)
            );

            return context.sync().then(() => {

            });
        })
            .catch((error) => {
                this.deleteListOfAttachments();
                let listSetting = this._formattingListSettingsService.getAttachmentsListSettings();
                listSetting === 'SELECTED' ? this.addListOfAttachmentsSelected() : this.addListOfAttachmentsAtTheEnd();
                console.log('Error: ' + JSON.stringify(error));
                if (error instanceof OfficeExtension.Error) {
                    console.log('Debug info: ' + JSON.stringify(error.debugInfo));
                }
            });
    }

    deleteListOfAttachments(): void {
        Word.run((context: any) => {
            const contentController = context.document.contentControls.getByTag('listOfAttachments');
            context.load(contentController, 'text');

            return context.sync().then(() => {
                if (contentController.items.length === 0) {
                    console.log("There isn't a content control with a tag of binding in this document.");
                } else {
                    contentController.items.forEach((controller: any) => {
                        controller.delete(true);
                    });
                    return context.sync().then(() => {
                    });
                }
            });
        });
    }

    updateListOfAttachments() {
        Word.run((context: any) => {
            const contentController = context.document.contentControls.getByTag('listOfAttachments');
            context.load(contentController, 'text');

            return context.sync().then(() => {
                if (contentController.items.length === 0) {
                    console.log("There isn't a content control with a tag of binding in this document.");
                } else {

                    const HTMLList = this.getHtmlListOfAttachments();
                    contentController.items.forEach((controller: any) => {
                        controller.insertHtml(HTMLList, 'replace');
                    });

                    this._blobService.updateServerNames().subscribe(
                        (data: any) => console.log(data),
                        (err: any) => console.error(err)
                    );

                    return context.sync().then(() => {
                    });
                }
            });
        });
    }

    getContentOfAttachment(attachment: Attachment): string {
        const formattingSettings = this._formattingSettingService.getFormattingSettings();
        const regNumberVar = formattingSettings.attNumber.value + " \\d+";
        const regAttNumber = new RegExp(regNumberVar, 'g');
        return attachment.content
            .replace(formattingSettings.beforeDesc.value, "")
            .replace(formattingSettings.afterDesc.value, "")
            .replace(regAttNumber, "")
            .trim();
    }

    getNumberOfAttachment(attachment: Attachment): number {
        const formattingSettings = this._formattingSettingService.getFormattingSettings();
        const regNumberVar = formattingSettings.attNumber.value + " \\d+";
        const regAttNumber = new RegExp(regNumberVar, 'g');
        return Number(attachment.content
            .match(regAttNumber)
            .toString()
            .match(" \\d+"))
    }


    getFormattedListOfAttachments(attachments: Array<Attachment>): Array<string> {
        const formattingListSettings = this._formattingListSettingsService.getFormattingListSettings();
        return attachments.reduce((prev: Array<string>, attachment: Attachment) => {
            const content = this.getContentOfAttachment(attachment);
            const index = this.getNumberOfAttachment(attachment) - 1;
            return [...prev, createHtml(formattingListSettings, content, index, 'BEFORE_DESC').insert];
        }, [])
    }

    getHtmlListOfAttachments(): string {
        const attachments = this._containerService.getAttachments();
        let HTMLList = '<ol>';
        const attachmentsHtml = this.getFormattedListOfAttachments(attachments);
        attachmentsHtml.map((att: string) => HTMLList += '<li>' + att + '</li>');
        HTMLList += '</ol>';
        return HTMLList;
    }

}