import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { globalUrl } from '../../common/globalUrl';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginService {
    constructor (private _http: Http) { }

    authenticate(documentDir: string,packageSize:any): Observable<any> {
        const headers = new Headers();
        headers.set('content-type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        const body={documentDir,packageSize};
        return this._http.post(globalUrl("create-document"),body,options)
            .map(res => res.json());
    }
}
