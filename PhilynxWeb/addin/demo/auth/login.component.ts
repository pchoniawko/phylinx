import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { DocumentSettingsService } from '../../common/services/document-settings.service';

@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    optionsList = [3, 5, 10, 15, 20];
    packageSize: number;
    documentDir: string;

    constructor(private _loginService: LoginService,
                private documentSettingsService: DocumentSettingsService,) { }


    ngOnInit() {
        this.packageSize = this.optionsList[1];
        this.documentDir = "";
    }


    authenticate(): void {
        this._loginService.authenticate(this.documentDir, this.packageSize)
            .subscribe(
                (data) => {
                    localStorage.setItem("logged", data.documentDir);
                    localStorage.setItem("packageSize", data.packageSize);
                    Office.context.document.settings.set("logged", data.documentDir);
                    Office.context.document.settings.set("packageSize", data.packageSize);
                    this.documentSettingsService.persistDocumentSettings();
                    console.log(data.documentDir);
                },
                (err) => {
                    localStorage.removeItem("logged");
                    localStorage.removeItem("packageSize");

                    Office.context.document.settings.remove("logged");
                    Office.context.document.settings.remove("packageSize");
                    this.documentSettingsService.persistDocumentSettings();

                }
            );
    }
}
