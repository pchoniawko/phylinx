import { Component, OnInit, NgZone } from '@angular/core';
import { Attachment } from '../../common/models/attachment';
import { IBindingResult } from '../../office/ibinding-result';
import { WordService } from '../../common/services/word.service';
import { ContainerService } from '../../common/services/container.service';
import { I18nService } from '../../common/i18n/i18n.service';
import { DocumentSettingsService } from '../../common/services/document-settings.service';
import { FileService } from '../../common/services/file.service';
import {GenerateDocumentService} from '../generate-document/generate-document.service';
import { OfficeInteractionService } from './office-interaction.service';
import { BlobService } from '../../blob/blob.service';

declare var htmlHelper: any;

@Component({
    moduleId: module.id,
    templateUrl: './office-interaction.component.html',
    styleUrls: ['./office-interaction.component.css']
})


export class OfficeInteractionComponent implements OnInit{

    title: string;
    description: string;
    htmlHelper: any;
    isBusy: boolean;
    userInput: string;
    alertMessage: string;
    currentTab: number;
    added: boolean;


    constructor(private zone: NgZone,
                private wordService: WordService,
                public container: ContainerService,
                private _i18n: I18nService,
                private fileService: FileService,
                private documentSettingsService: DocumentSettingsService,
                private generateDocumentService:GenerateDocumentService,
                private _officeInteractionService: OfficeInteractionService,
                private _blobService: BlobService) {
        
        sessionStorage.removeItem('blob');           

        this.title = this._i18n.translate("ATTACHMENTS_PANEL");
        this.description = this._i18n.translate("ASSIGN_DESCRIPTION");

        container.dataLogger$.subscribe((value: string) => {
            this.alertMessage = value;
        });
    }



    ngOnInit() {
        
        this.currentTab = 0;
        this.added = false;
        this.wordService.getStoredAttachments().subscribe(value => {
            this.zone.run(() => {
                this.container.addAttachment(value);
            });
        });

        this.wordService.data$.subscribe(
            data => {
                this.zone.run(() => this.container.updateAttachment(data));
            });
    }

    attachmentClicked(attachment: Attachment): void {
        this.wordService
            .highlightAttachment(attachment)
                .then((result: IBindingResult) => console.log(attachment))
                .catch((error: any) => null);
    }

    connectFile(attachment: Attachment): void {
        var element = document.getElementById('file-' + attachment.id);
        if (element) {
            element.click(); // run input type file
        }
    }
    showLocalStorage(){
        console.log("LocalStorage:",localStorage);
    }
    clearLocalStorage(){
        localStorage.clear();
    }
    
    changeTabs(tab: number) {
        this.currentTab = tab;
        
    }
    
    addedNewItem(event: any) {
        this.added = true;
        setTimeout(() => { this.added = false }, 2000);
    }

    showContentControllers(){
        Office.context.document.bindings.getAllAsync((asyncResult:any)=> {
            console.log("asyncResult",asyncResult);
            const re = /0+-/g;
            asyncResult.value
                    .filter((element:any)=>{
                        const i= element.id.search(re);
                        if (i!==-1){
                        return element;
                        }})
                    .map((element:any)=>{console.log(element)});
        });
        Word.run((context: any) => {

            const contentControls = context.document.contentControls;
            context.load(contentControls, ['text','tag']);
            return context.sync().then(() => {
                // const items = contentControlsBinding.items.concat(contentControlsRelated.items);
                    console.log("contentControls.items",contentControls.items);
            });
        });
    }

    /**
     * Delete binding from document
     * @param attachment
     */
    deleteAttachment(attachment: Attachment): void {
        attachment.filePath = '';
        this.container.removeAttachment(attachment);
        this.wordService.removeBindingByAttachment(attachment).then((result: any) => {
            this.documentSettingsService.removeDocumentSettings(attachment.id);
            this.documentSettingsService.persistDocumentSettings();
            this.wordService.deleteContentControl(attachment).then(()=>{
                    this.wordService.checkAttNumber();
            });
            this._officeInteractionService.deleteFile(attachment.serverName)
                .subscribe(
                    data => {
                        console.log({data});
                    }
                );
        });
    }

    /**
     * Save selected file into attachment.
     * @param attachment
     */


    fileSelected = (attachment: Attachment, e: Event): any => {
        const element = <HTMLInputElement>e.target;
        if (element) {
            const file = element.files[0];
            const objectUrl = URL.createObjectURL(file);
            attachment.filePath = objectUrl;
            this.documentSettingsService.setDocumentSettings(attachment.id, attachment.toJSON());
            this.documentSettingsService.persistDocumentSettings();
            const reader = new FileReader();
                 reader.onloadend = () => {

                    this._officeInteractionService.sendAttachment(reader.result, attachment)
                        .subscribe(
                            (data) => {
                                attachment.serverName = data.fileName;
                                this.documentSettingsService.setDocumentSettings(attachment.id, attachment.toJSON());
                                this.documentSettingsService.persistDocumentSettings();
                            },
                            (error) => console.log(`Error: ${error}`)
                        );    
                };
                     reader.readAsDataURL(file);
        }
    };

    downloadAttachment(attachment: Attachment): void {
        if (Office.context.document.settings.get('attachmentSetting') === 'IN') {
            this.fileService.downloadFileFromAttachment(attachment);
        } else {
            console.log('attachment.serverName', attachment.serverName);
            this._blobService.downloadFile(attachment.serverName)
                .subscribe(
                    (data) => { 
                        console.log(data);
                        sessionStorage.removeItem('blob');
                    },
                    (err) => sessionStorage.removeItem('blob')
                );
            };
    }

    resetAlertMessage(): void {
        this.alertMessage = '';
    }
}
