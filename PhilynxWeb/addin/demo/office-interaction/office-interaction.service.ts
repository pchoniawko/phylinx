import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

import { Http, Headers, RequestOptions } from '@angular/http';
import { globalUrl } from '../../common/globalUrl';
import { Attachment } from '../../common/models/attachment';
import  convertName from '../../common/convertName';
import { loadStateString } from '../../common/loadState';
import { Settings } from '../settings/settings.component';


@Injectable()
export class OfficeInteractionService {

    constructor(private _http: Http) { }

    sendAttachment = (content: string, att: Attachment): Observable<any> => {
        const headers = new Headers();
        headers.set('content-type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        const pureName = convertName(att.content);
        const body = JSON.stringify({
            file: content,
            name: pureName,
            documentDir: loadStateString("logged"),
            packageSize: loadStateString("packageSize")
        });
        return this._http.post(globalUrl("save-file"), body, options)
            .map(res => res.json());
    }

    deleteFile(fileName: string): Observable<any> {
        const headers = new Headers();
        headers.set('content-type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        const body = {
            fileName: fileName,
            documentDir: loadStateString("logged")
        };
        return this._http.post(globalUrl(`delete-file`),body,options)
            .map(res => res.json());
    }

    clearOfficeSettings(): void {
        Office.context.document.settings.remove('bindingSettings');
        Office.context.document.settings.remove('formattingSettings');
        Office.context.document.settings.saveAsync();

    }
}

