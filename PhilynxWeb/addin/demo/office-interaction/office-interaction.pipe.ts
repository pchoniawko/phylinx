/**
 * Created by Paweł on 05.07.2017.
 */
import { Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'search'
})

export class SearchPipe implements PipeTransform {
    transform(value:any, keys: string, term: string) {

        return term ? (value || []).filter((item:any) => keys.split(',')
                                   .some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi')
                                   .test(item[key]))) :
                   value;

    }
}