import { Injectable } from '@angular/core';
import { loadStateString } from '../../common/loadState';
import { Http, Headers, RequestOptions } from '@angular/http';
import { globalUrl } from '../../common/globalUrl';



import 'rxjs/add/operator/map';


@Injectable()
export class PackFilesService {

    constructor(private _http: Http) { }
    packFiles(){
        const headers = new Headers();
        headers.set('content-type', 'application/json');
        const body = {
            packageSize:loadStateString("packageSize"),
            documentDir:loadStateString("logged")
        };
        let options = new RequestOptions({ headers: headers });
        return this._http.post(globalUrl(`pack-files`),body,options)
            .map((res: any) => res._body);
    }

}