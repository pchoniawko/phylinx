import { Component, OnInit } from '@angular/core';
import {PackFilesService } from './pack-files.service';

@Component({
    moduleId: module.id,
    selector: 'pack-files',
    templateUrl: './pack-files.component.html',
    styleUrls: ['./pack-files.component.css']
})
export class PackFilesComponent implements OnInit {
    constructor(private packFilesService: PackFilesService) { }

    ngOnInit() { }

        packFiles(): void {
        this.packFilesService.packFiles().subscribe(
            (data:any) => console.log(data),
            (err:any) => console.error(err)
        );
    }
}
