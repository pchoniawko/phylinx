import { Component, OnInit } from '@angular/core';
import { IFormattingSettings } from '../formatting-settings/IFormattingSettings';
import { FormattingSettingsService } from '../formatting-settings/formatting-settings.service';
import { I18nService } from '../../common/i18n/i18n.service';
import {WordService} from '../../common/services/word.service';



@Component({
    moduleId: module.id,
    selector: 'select-formatting',
    templateUrl: './select-formatting.component.html',
    styleUrls: ['./select-formatting.component.css']
})
export class SelectFormattingComponent implements OnInit {
    newFormattingsSettings: any;
    formattingsList: any;
    currentFormattings: IFormattingSettings; 
    showAlert: boolean;
    alertMessage: string;
    
    constructor(private _formattingSettingsService: FormattingSettingsService,
                private _i18n: I18nService,
                private  _wordService:WordService) {
        this.alertMessage = _i18n.translate('CHANGE_FORMATTING');
    }

    ngOnInit() {
        this.getFormattingsList();
        this.getCurrentFormattingSettings();
     }

    getFormattingsList(): void {
       this.formattingsList = this._formattingSettingsService.getFormattingSettingsCollection();
        console.log("this.formattingsList",this.formattingsList);

    }
    changeFormattingsSettings() {
        this.showAlert = true;
        this._wordService.checkAttNumber(this.newFormattingsSettings).then(()=>{
            this._formattingSettingsService.saveFormattingSettings(this.newFormattingsSettings);
        });
    }

    onClose(): void {
        this.showAlert = false;
    }
    
    getCurrentFormattingSettings(): void {
        this.currentFormattings = this._formattingSettingsService.getFormattingSettings();
    }
}