import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { WordService } from '../../common/services/word.service';
import { IOfficeResult } from '../../office/ioffice-result';
import { ContainerService } from '../../common/services/container.service';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'assign',
    templateUrl: './assign.component.html',
    styleUrls: ['./assign.component.css']
})
export class AssignComponent implements OnInit {

    disabled: boolean;
    
    @Output() add = new EventEmitter();

    constructor(private _wordService: WordService,
                private _containerService: ContainerService,
                private _router: Router
    ) { }

    ngOnInit() { 
    
    }

    assign(): void {
        if (Office.context.document.settings.get('formattingSettings') ||
            localStorage.getItem('formattingSettings')) {
                this.disabled = true;
                this._wordService.setSelectedTextAsAttachment()
                    .then((result: IOfficeResult) => {
                        this.add.emit({});
                        this._containerService.addAttachment(result.value);
                        this.disabled = false;
                        console.log("emitting an event");
                        
                        
                    }).catch((error: IOfficeResult)=> {
                        this.disabled = false;
                    });
        } else {
            this._router.navigate(['/formatting-settings']);
        }
    }
}
