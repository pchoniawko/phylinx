import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { I18nService } from '../../../common/i18n/i18n.service';
import { IFormattingSettings } from '../IFormattingSettings';

@Component({
    moduleId: module.id,
    selector: 'formatting-settings-item',
    templateUrl: './formatting-settings-item.component.html',
    styleUrls: [ './formatting-settings-item.component.css' ],
})
export class FormattingSettingsItemComponent implements OnInit {
    @Input() label: string;
    @Input() item: string;
    @Input() formattingSettings: IFormattingSettings;
    @Output() changeSetting = new EventEmitter();

    title: string;
    isSettingsSaved: boolean = false;
    constructor(private _i18n: I18nService) { }

    ngOnInit() {
        this.title = this._i18n.translate('FORMATTING_SETTINGS');
    }

    changeFormattingSettings(formattingSettings: IFormattingSettings): void {
        this.changeSetting.emit({
            value: formattingSettings
        });
    }
}
