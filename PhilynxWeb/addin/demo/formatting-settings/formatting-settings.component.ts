import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {I18nService} from '../../common/i18n/i18n.service';
import {IFormattingSettings} from './IFormattingSettings';
import {Router} from '@angular/router';
import {FileService} from '../../common/services/file.service';
import {FormattingSettingsService} from './formatting-settings.service';
import {defaultFormattingSettings} from './default-settings-controls';
import {WordService} from '../../common/services/word.service';

@Component({
    moduleId: module.id,
    templateUrl: './formatting-settings.component.html',
    styleUrls: ['./formatting-settings.component.css'],
})
export class FormattingSettingsComponent implements OnInit {
    @ViewChild('settingsFile') el: ElementRef;
    title: string;
    isSettingsSaved: boolean = false;
    formattingSettings: IFormattingSettings | null;
    labels: string[];
    items: string[];
    formattingCollection: any;
    chosenFormattingSetting: any;
    formattingName: string;
    showAlert:boolean;
    alertMessage:string;

    constructor(public _i18n: I18nService,
                private _router: Router,
                private _fileService: FileService,
                private _formattingSettingSevice: FormattingSettingsService,
                private _wordService:WordService) {
        this.title = this._i18n.translate('ATTACHMENT_SETTINGS_TITLE');
        this.showAlert = false;
    }

    ngOnInit() {
        this.title = this._i18n.translate('FORMATTING_SETTINGS');

        this.formattingSettings = Object.assign({}, this._formattingSettingSevice.getFormattingSettings());
        this.formattingCollection = [...this._formattingSettingSevice.getFormattingSettingsCollection(), defaultFormattingSettings];
        this.items = Object.keys(this.formattingSettings);
        this.formattingName = this.formattingSettings.name;

        this.labels = [
            'LABEL_BEFORE_DESC',
            'LABEL_ATTACHMENT_NUMBER',
            'LABEL_AFTER_DESC'
        ];

    }
    onClose(): void {
        this.showAlert = false;
    }


    choseFormatting(formatting: IFormattingSettings) {
        this.formattingSettings = JSON.parse(JSON.stringify(formatting));
        this.formattingName = this.formattingSettings.name;
    }

    deleteFormatting() {
        this.showAlert = true;
        this.alertMessage = 'Formatting deleted';
        this._formattingSettingSevice.deleteFormattingFromFormattingCollection(this.formattingSettings);
        this.formattingCollection = [...this._formattingSettingSevice.getFormattingSettingsCollection(), defaultFormattingSettings];
        if (this.formattingSettings.name === this._formattingSettingSevice.getFormattingSettings().name) {
            this.formattingSettings = JSON.parse(JSON.stringify(defaultFormattingSettings));
            this.formattingName = this.formattingSettings.name;
            this._wordService.checkAttNumber(this.formattingSettings).then(()=>{
                this._formattingSettingSevice.saveFormattingSettings(this.formattingSettings).then(() => {
                    this.isSettingsSaved = true;
                });
            });
        } else {
            this.formattingSettings = JSON.parse(JSON.stringify(defaultFormattingSettings));
            this.formattingName = this.formattingSettings.name;
        }
    }

    saveSettings() {
        this.showAlert = true;
        this.alertMessage = 'Formatting saved';
        this.formattingSettings.name = this.formattingName;
        this._wordService.checkAttNumber(this.formattingSettings).then(()=>{
            this._formattingSettingSevice.saveFormattingSettings(this.formattingSettings).then(() => {
                this.isSettingsSaved = true;
            });
            this._formattingSettingSevice.saveFormattingSettingsToCollection(this.formattingSettings);
            this.formattingCollection = [...this._formattingSettingSevice.getFormattingSettingsCollection(), defaultFormattingSettings];
        });

    }

    changeSettings(settings: IFormattingSettings) {
        console.log('settings', settings);
        this.formattingSettings = settings;
    }

    downloadSettings() {
        this._fileService.downloadFileFromSettings(this._formattingSettingSevice.getFormattingSettings());
    }


}