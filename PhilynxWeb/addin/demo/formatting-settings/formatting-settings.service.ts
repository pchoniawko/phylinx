import { Injectable } from '@angular/core';
import { IFormattingSettings } from './IFormattingSettings';
import { DocumentSettingsService } from '../../common/services/document-settings.service';
import { defaultFormattingSettings } from './default-settings-controls';
import { loadState,loadStateString } from '../../common/loadState';

@Injectable()
export class FormattingSettingsService {
    constructor(private _documentSettingsService: DocumentSettingsService) { }

    getFormattingSettings(): IFormattingSettings {
        return loadState('formattingSettings') ? loadState('formattingSettings') : defaultFormattingSettings;
    }

    getDefaultFormattingSettings(): IFormattingSettings {
        return defaultFormattingSettings;
    }



    saveFormattingSettings(settings: IFormattingSettings): Promise<any> {
        return new Promise((resolve, reject) => {
            Office.context.document.settings.set('formattingSettings', JSON.stringify(settings));
                localStorage.setItem('formattingSettings', JSON.stringify(settings));
                this._documentSettingsService.persistDocumentSettings();
                resolve();
        }).then(() => {
            this._documentSettingsService.persistDocumentSettings();
        });       
    }


    saveFormattingSettingsToCollection(settings: IFormattingSettings){
            if (loadState('formattingSettingsCollection')) {
                this.addToFormattingCollectionSettings(settings);
            } else {
                this.createFormattingCollectionSettings(settings);
            }
    }

    createFormattingCollectionSettings(settings: IFormattingSettings):void{
        let formattingSettingsCollection=[];
        formattingSettingsCollection.push(settings);
        localStorage.setItem('formattingSettingsCollection',JSON.stringify(formattingSettingsCollection));
        Office.context.document.settings.set('formattingSettingsCollection', JSON.stringify(formattingSettingsCollection));
        this._documentSettingsService.persistDocumentSettings();
    }

    addToFormattingCollectionSettings(settings: IFormattingSettings):void{
        let formattingSettingsCollection = loadState('formattingSettingsCollection');
        formattingSettingsCollection = formattingSettingsCollection
            .filter((formatting:IFormattingSettings)=>formatting.name!==settings.name);
        formattingSettingsCollection.push(settings);
        localStorage.setItem('formattingSettingsCollection',JSON.stringify(formattingSettingsCollection));
        Office.context.document.settings.set('formattingSettingsCollection', JSON.stringify(formattingSettingsCollection));
        this._documentSettingsService.persistDocumentSettings();
    }

    deleteFormattingFromFormattingCollection(settings: IFormattingSettings):void{
        let formattingSettingsCollection = loadState('formattingSettingsCollection');
        formattingSettingsCollection = formattingSettingsCollection
            .filter((formatting:IFormattingSettings)=>formatting.name!==settings.name);
        localStorage.setItem('formattingSettingsCollection',JSON.stringify(formattingSettingsCollection));
        Office.context.document.settings.set('formattingSettingsCollection', JSON.stringify(formattingSettingsCollection));
        this._documentSettingsService.persistDocumentSettings();

    }

    getFormattingSettingsCollection(): any {
        return loadState('formattingSettingsCollection') ? loadState('formattingSettingsCollection') : [];
    }
}