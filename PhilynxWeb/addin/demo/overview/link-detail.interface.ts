export interface ILinkDetail {
    route: string;
    title: string;
}