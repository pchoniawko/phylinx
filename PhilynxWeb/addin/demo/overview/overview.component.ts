import { Component, OnInit } from '@angular/core';
import { I18nService } from '../../common/i18n/i18n.service';
import { ILinkDetail } from './link-detail.interface';
import {loadStateString} from '../../common/loadState'


@Component({
  moduleId: module.id,
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})

export class OverviewComponent implements OnInit {
  title: string;
  description: string;

  details: ILinkDetail[];

  constructor(private _i18n: I18nService) {
    
    this.title = this._i18n.translate('OVERVIEW');
    this.description = this._i18n.translate('START_PHILYNX');
    this.details = [
      { route: '/office',              title: this._i18n.translate('ATTACHMENTS') },
      { route: '/timeline-overview',            title: this._i18n.translate('TIMELINE')},
      { route: '/summary',             title: 'Summary'},
      {route: '/formatting-overview',  title: this._i18n.translate('SETTINGS')}
    ];
    
  }

  ngOnInit() { }
  
  isLogged = (): boolean => loadStateString("logged") ? true : false;
}