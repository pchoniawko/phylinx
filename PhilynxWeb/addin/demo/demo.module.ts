import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { FormsModule } from '@angular/forms';

import { demoRouting } from './demo.routing';

import { OverviewComponent } from './overview/overview.component';
import { OfficeInteractionComponent } from './office-interaction/office-interaction.component';

import { OfficeModule } from '../office/office.module';
import { OfficeUiModule } from '../office-ui/office-ui.module';

import { ContainerService } from '../common/services/container.service';
import { WordService } from '../common/services/word.service';
import { FileService } from '../common/services/file.service';
import { DocumentSettingsService } from '../common/services/document-settings.service';
import { I18nService } from '../common/i18n/i18n.service';
import { FormattingSettingsComponent } from './formatting-settings/formatting-settings.component';
import { FormattingListSettingsComponent } from './formatting-list-settings/formatting-list-settings.component';
import { I18nPipe } from '../common/i18n/i18n.pipe';
import { SettingsControlsComponent } from '../common/settings-controls/settings-controls.component';
import { SettingsControlsService } from '../common/settings-controls/settings-controls.service';
import { ToogleComponent } from '../common/toogle/toogle.component';
import { GenerateDocumentComponent } from './generate-document/generate-document.component';
import { GenerateDocumentService } from './generate-document/generate-document.service';
import { PackFilesComponent } from './pack-files/pack-files.component';
import { PackFilesService } from './pack-files/pack-files.service';
import { HttpModule } from '@angular/http';
import { OfficeInteractionService } from './office-interaction/office-interaction.service';
import { FormattingSettingsService } from './formatting-settings/formatting-settings.service';
import { FormattingListSettingsService } from './formatting-list-settings/formatting-list-settings.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { BlobModule } from '../blob/blob.module';
import { EndListComponent } from './end-list/end-list.component';
import { EndListService } from './end-list/end-list.service';
import { AssignComponent } from './assign/assign.component';
import { FormattingSettingsItemComponent } from './formatting-settings/formatting-settings-item/formatting-settings-item.component';
import { FormattingListSettingsItemComponent } from './formatting-list-settings/formatting-list-settings-item/formatting-list-settings-item.component';
import { SelectFormattingComponent } from './select-formatting/select-formatting.component';
import { SelectListFormattingComponent } from './select-list-formatting/select-list-formatting.component';
import { AlertMessage } from './alert-message/alert.component';
import { FormattingOverview } from './formatting-overview/formatting-overview.component';
import { AttachmentSettingsOverview } from './attachment-settings-overview/attachment-settings-overview.component';
import { ListSettingsOverview } from './list-settings-overview/list-settings-overview.component';
import { Settings } from './settings/settings.component'
import { SearchPipe} from './office-interaction/office-interaction.pipe';
import { LoginComponent } from './auth/login.component';
import { LoginService } from './auth/login.service';
import { SummaryComponent } from './summary/summary.component';
import { ListSettingsComponent } from './list-settings/list-settings.component';
import { TimelineComponent } from './timeline/timeline.component';
import { TimelineOverview } from './timeline-overview/timeline-overview.component';
import { StampUploadComponent } from './stamp-upload/stamp-upload.component';
import { StampUploadService } from './stamp-upload/stamp-upload.service';

import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { TimelineInput } from './timeline-input/timeline-input.component';
import { TimelineService } from './timeline-overview/timeline-overview.service';
import { DateService } from './timeline-overview/date.service';
import { ClearOfficeSettingsComponent } from './clear-office-settings/clear-office-settings.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        demoRouting,
        OfficeModule,
        OfficeUiModule,
        HttpModule,
        BlobModule,
        Angular2FontawesomeModule
    ],
    providers: [
        WordService,
        ContainerService,
        FileService,
        DocumentSettingsService,
        I18nService,
        SettingsControlsService,
        GenerateDocumentService,
        PackFilesService,
        OfficeInteractionService,
        FormattingSettingsService,
        FormattingListSettingsService,
        CookieService,
        EndListService,
        LoginService,
        StampUploadService,
        TimelineService,
        DateService
    ],
    declarations: [
        OverviewComponent, 
        OfficeInteractionComponent,
        FormattingSettingsComponent,
        FormattingListSettingsComponent,
        I18nPipe,
        SettingsControlsComponent,
        ToogleComponent,
        GenerateDocumentComponent,
        PackFilesComponent,
        EndListComponent,
        AssignComponent,
        FormattingSettingsItemComponent,
        FormattingListSettingsItemComponent,
        SelectFormattingComponent,
        SelectListFormattingComponent,
        AlertMessage,
        FormattingOverview,
        AttachmentSettingsOverview,
        ListSettingsOverview,
        Settings,
        SearchPipe,
        LoginComponent,
        SummaryComponent,
        ListSettingsComponent,
        TimelineComponent,
        TimelineOverview,
        StampUploadComponent,
        TimelineInput,
        ClearOfficeSettingsComponent
    ],
    exports: [
        OverviewComponent, 
        OfficeInteractionComponent,
        FormattingSettingsComponent,
        FormattingListSettingsComponent,
        I18nPipe,
        SearchPipe,
        SettingsControlsComponent,
        ToogleComponent,
        GenerateDocumentComponent,
        PackFilesComponent,
        EndListComponent,
        AssignComponent,
        FormattingSettingsItemComponent,
        FormattingListSettingsItemComponent,
        SelectFormattingComponent,
        SelectListFormattingComponent,
        FormattingOverview,
        AttachmentSettingsOverview,
        ListSettingsOverview,
        Settings,
        LoginComponent,
        ListSettingsComponent,
        TimelineComponent,
        TimelineOverview,
        StampUploadComponent,
        TimelineInput,
        ClearOfficeSettingsComponent
    ]
})
export class DemoModule {
}