import { Component, OnInit } from '@angular/core';
import { GenerateDocumentService } from './generate-document.service';
import { ContainerService } from '../../common/services/container.service';

@Component({
    moduleId: module.id,
    selector: 'generate-document',
    templateUrl: './generate-document.component.html',
    styleUrls: ['./generate-document.component.css']
})
export class GenerateDocumentComponent implements OnInit {

    constructor(private _generateDocumentService: GenerateDocumentService,
                private _containerService: ContainerService) { }

    ngOnInit() { }

    generateDoc(): void {
        const attachments = this._containerService.getAttachments();
        this._generateDocumentService.generateConcatContent(attachments)
            .then(
                (res: string) => this._generateDocumentService.createDoc(res)
            );  
    }
}
