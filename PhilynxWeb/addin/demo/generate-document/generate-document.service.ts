import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Attachment} from "../../common/models/attachment";

const jszipUtilis = require('/Scripts/jszip-utils.js');
const htmlDocx = require('/Scripts/html-docx.js');
const mammoth = require('/node_modules/mammoth/mammoth.browser.js');
import 'rxjs/add/operator/do';

@Injectable()
export class GenerateDocumentService {
    
    constructor(private _http: Http) { }

    generateConcatContent(attachments: Attachment[]): Promise<string> {
        return Promise.all(
            attachments
                .filter((att) => att.filePath !== '')
                .map((att) => this.loadFile(att.filePath)))
            .then(res => res.join("\n\n"));
    }

    loadFile(url: string): Promise<string> {
        return new Promise(resolve => {
            jszipUtilis.getBinaryContent(url, (error: any, content: any) => {
                if (error) { 
                    throw error;
                }
                mammoth.convertToHtml({arrayBuffer: content})
                    .then((result: any) => resolve(result.value))
                    .done();
            });
        });
    }

    createDoc(text: string): void {
        const content = '<!DOCTYPE html><html><body>'+text+'</body></html>',
        converted = htmlDocx.asBlob(content),
        link = window.document.createElement('a');

        link.href = URL.createObjectURL(converted);
        link.download = 'document.docx';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        URL.revokeObjectURL(converted);
    }

}