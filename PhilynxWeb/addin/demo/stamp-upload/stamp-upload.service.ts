import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { globalUrl } from '../../common/globalUrl';

@Injectable()
export class StampUploadService {

    constructor(private _http: Http) { }

    sendStamp(stamp: any) {
        const headers = new Headers();
        headers.set('content-type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        const body = JSON.stringify(stamp);

        return this._http.post(globalUrl('stamp-upload'), body, options)
            .map(res => res.json());
    }
}