import { Component, OnInit } from '@angular/core';
import { StampUploadService } from './stamp-upload.service';

@Component({
    moduleId: module.id,
    selector: 'stamp-upload',
    templateUrl: './stamp-upload.component.html',
    styleUrls: ['./stamp-upload.component.css']
})
export class StampUploadComponent implements OnInit {
    stamp: { fileName: string, file: string } = {fileName: "", file: ""};

    constructor(private _stampUploadService: StampUploadService) { }

    ngOnInit() { }

    fileSelected(e: Event) {
        const element = <HTMLInputElement>e.target;
            const file = element.files[0];
            const objectUrl = URL.createObjectURL(file);
            const reader = new FileReader();
            console.log(objectUrl);
            reader.onloadend = () => {
                this.stamp.file = reader.result;
                console.log({objectUrl, reader: reader.result});
            };
            reader.readAsDataURL(file);
    }

    sendFile() {
        if (this.stamp.file) {
            this._stampUploadService.sendStamp(this.stamp)
                .subscribe(
                    () => {
    
                    }
                );
        }
    }
}
