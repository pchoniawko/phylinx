import { Component, OnInit } from '@angular/core';
import { I18nService } from '../../common/i18n/i18n.service';
import { ILinkDetail } from '../overview/link-detail.interface';
import {DocumentSettingsService} from '../../common/services/document-settings.service';


@Component({
    moduleId: module.id,
    selector: 'list-settings-overview',
    templateUrl: './list-settings-overview.component.html',
    styleUrls: [ './list-settings-overview.component.css' ]
})
export class ListSettingsOverview implements OnInit {
   details: ILinkDetail[];
    startAttNumber:any;
  constructor(private _i18n: I18nService,
              private DocumentSettingsService:DocumentSettingsService) {

      this.details = [
      { route: '/select-list-formatting',   title: 'Select list formatting' },
      { route: '/formatting-list-settings',       title: 'Create new formatting list'},
      { route: '/list-settings',       title: 'List position'}
    ];
  }

    ngOnInit() { }

}
