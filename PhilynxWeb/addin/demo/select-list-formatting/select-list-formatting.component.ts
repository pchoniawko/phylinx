import { Component, OnInit } from '@angular/core';
import { IFormattingListSettings } from '../formatting-list-settings/IFormattingListSettings';
import { FormattingListSettingsService } from '../formatting-list-settings/formatting-list-settings.service';
import { I18nService } from '../../common/i18n/i18n.service';


@Component({
    moduleId: module.id,
    selector: 'select-list-formatting',
    templateUrl: './select-list-formatting.component.html',
    styleUrls: ['./select-list-formatting.component.css']
})
export class SelectListFormattingComponent implements OnInit {
    newFormattingsSettings: any;
    formattingsList: any;
    currentFormattings: IFormattingListSettings;
    showAlert: boolean;
    alertMessage: string;
    
    constructor(private _formattingListSettingsService: FormattingListSettingsService, private _i18n: I18nService) {
        this.alertMessage = _i18n.translate('CHANGE_FORMATTING');
    }

    ngOnInit() {
        this.getFormattingsList();
        this.getCurrentFormattingListSettings();
     }

    getFormattingsList(): void {
       this.formattingsList = this._formattingListSettingsService.getFormattingListSettingsCollection();
        console.log("this.formattingsList",this.formattingsList);
    }

    changeFormattingListSettings():void{
        this.showAlert = true;
        this._formattingListSettingsService.saveFormattingListSettings(this.newFormattingsSettings);
    }
    onClose(): void {
        this.showAlert = false;
    }
    
    getCurrentFormattingListSettings(): void {
        this.currentFormattings = this._formattingListSettingsService.getFormattingListSettings();
    }
}