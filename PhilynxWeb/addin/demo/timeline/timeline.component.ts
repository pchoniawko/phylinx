import {Component, OnInit} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'timeline',
    templateUrl: './timeline.component.html',
    styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {
    
    events: any[];
    currentTab: number;
    eventName: string;

    ngOnInit() {
        this.currentTab = 0;
        this.events = [];
    }
    
     changeTabs(tab: number) {
        this.currentTab = tab;
    }
    
    createEvent(d: string, n: string) {
        this.events.push({date: d, name: n});
        console.log(this.events);
    }
    

}