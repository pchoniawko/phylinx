import { Component, OnInit } from '@angular/core';
import { FormattingListSettingsService } from '../formatting-list-settings/formatting-list-settings.service';


@Component({
    moduleId: module.id,
    selector: 'list-settings',
    templateUrl: './list-settings.component.html',
    styleUrls: ['./list-settings.component.css']
})
export class ListSettingsComponent implements OnInit {

    selectedListOption: string;
    options: string[] = [];
    
    constructor( private _formattingListSettingService: FormattingListSettingsService) { }

    ngOnInit() {
        this.options = [
            'END',
            'SELECTED'
        ];
        this.selectedListOption = this._formattingListSettingService.getAttachmentsListSettings();
        if(this.selectedListOption===null){
            this.selectedListOption='END';
        }
    
    }
    
     saveSettings() {
        let savedOption;
        this.selectedListOption === 'END'? savedOption = 'END' : savedOption = 'SELECTED';
        this._formattingListSettingService.saveAttachmentsListSettings(savedOption);
        console.log(savedOption);
     }

  
}
