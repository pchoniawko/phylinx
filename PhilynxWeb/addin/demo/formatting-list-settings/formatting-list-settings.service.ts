import { Injectable } from '@angular/core';
import { IFormattingListSettings } from './IFormattingListSettings';
import { DocumentSettingsService } from '../../common/services/document-settings.service';
import { defaultFormattingListSettings } from './default-list-settings-controls';
import { loadState,loadStateString } from '../../common/loadState';

@Injectable()
export class FormattingListSettingsService {
    constructor(private _documentSettingsService: DocumentSettingsService) { }

    getFormattingListSettings(): IFormattingListSettings {
        return loadState('formattingListSettings') ? loadState('formattingListSettings') : defaultFormattingListSettings;
    }


    saveAttachmentsListSettings(setting: string):void{
            Office.context.document.settings.set('attachmentListSetting', setting);
            this._documentSettingsService.persistDocumentSettings();
            localStorage.setItem('attachmentListSetting', setting);
    }

    getAttachmentsListSettings(): string {
        return loadStateString('attachmentListSetting');
    }

    saveFormattingListSettings(settings: IFormattingListSettings): Promise<any> {
        return new Promise((resolve, reject) => {
            Office.context.document.settings.set('formattingListSettings', JSON.stringify(settings));
                localStorage.setItem('formattingListSettings', JSON.stringify(settings));
                this._documentSettingsService.persistDocumentSettings();
                resolve();
        }).then(() => {
            this._documentSettingsService.persistDocumentSettings();
        });       
    }


    saveFormattingListSettingsToCollection(settings: IFormattingListSettings){
            if (loadState('formattingListSettingsCollection')) {
                this.addToFormattingListCollectionSettings(settings);
            } else {
                this.createFormattingListCollectionSettings(settings);
            }
    }

    createFormattingListCollectionSettings(settings: IFormattingListSettings):void{
        let formattingSettingsCollection=[];
        formattingSettingsCollection.push(settings);
        localStorage.setItem('formattingListSettingsCollection',JSON.stringify(formattingSettingsCollection));
        Office.context.document.settings.set('formattingListSettingsCollection', JSON.stringify(formattingSettingsCollection));
        this._documentSettingsService.persistDocumentSettings();
    }

    addToFormattingListCollectionSettings(settings: IFormattingListSettings):void{
        let formattingSettingsCollection = loadState('formattingListSettingsCollection');
        formattingSettingsCollection = formattingSettingsCollection
            .filter((formatting:IFormattingListSettings)=>formatting.name!==settings.name);
        formattingSettingsCollection.push(settings);
        localStorage.setItem('formattingListSettingsCollection',JSON.stringify(formattingSettingsCollection));
        Office.context.document.settings.set('formattingListSettingsCollection', JSON.stringify(formattingSettingsCollection));
        this._documentSettingsService.persistDocumentSettings();
    }

    deleteFormattingFromFormattingListSettingsCollection(settings: IFormattingListSettings):void{
        let formattingSettingsCollection = loadState('formattingListSettingsCollection');
        formattingSettingsCollection = formattingSettingsCollection
            .filter((formatting:IFormattingListSettings)=>formatting.name!==settings.name);
        localStorage.setItem('formattingListSettingsCollection',JSON.stringify(formattingSettingsCollection));
        Office.context.document.settings.set('formattingListSettingsCollection', JSON.stringify(formattingSettingsCollection));
        this._documentSettingsService.persistDocumentSettings();
    }

    getFormattingListSettingsCollection(): any {
        return loadState('formattingListSettingsCollection') ? loadState('formattingListSettingsCollection') : [];
    }
}