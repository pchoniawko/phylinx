export interface IFormattingListSettings {
    name: string;
    beforeDesc: {
        value: string,
        fontWeight: string,
        fontStyle: string,
        textDecoration: string,
        fontFamily: string,
        fontSize: number
    };
    attNumber: {
        value: string,
        fontWeight: string,
        fontStyle: string,
        textDecoration: string,
        fontFamily: string,
        fontSize: number
    };
    afterDesc: {
        value: string,
        fontWeight: string,
        fontStyle: string,
        textDecoration: string,
        fontFamily: string,
        fontSize: number
    };
}