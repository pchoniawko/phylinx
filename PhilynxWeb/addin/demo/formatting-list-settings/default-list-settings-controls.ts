export const defaultFormattingListSettings= {
    name: "New Formatting Style",
    beforeDesc: {
        value: 'Załącznik nr ',
        fontWeight: 'normal',
        fontStyle: 'italic',
        textDecoration: 'none',
        fontFamily: 'Arial',
        fontSize: 10
    },
    attNumber: {
        value: 'Dowód',
        fontWeight: 'normal',
        fontStyle: 'italic',
        textDecoration: 'none',
        fontFamily: 'Arial',
        fontSize: 10
    },
    afterDesc: {
        value: '/',
        fontWeight: 'normal',
        fontStyle: 'italic',
        textDecoration: 'none',
        fontFamily: 'Arial',
        fontSize: 10
    }
};