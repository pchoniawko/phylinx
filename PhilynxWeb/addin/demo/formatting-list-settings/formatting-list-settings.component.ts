import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {I18nService} from '../../common/i18n/i18n.service';
import {IFormattingListSettings} from './IFormattingListSettings';
import {Router} from '@angular/router';
import {FileService} from '../../common/services/file.service';
import {FormattingListSettingsService} from './formatting-list-settings.service';
import {defaultFormattingListSettings} from './default-list-settings-controls';
@Component({
    moduleId: module.id,
    templateUrl: './formatting-list-settings.component.html',
    styleUrls: ['./formatting-list-settings.component.css'],
})
export class FormattingListSettingsComponent implements OnInit {
    @ViewChild('settingsFile') el: ElementRef;
    title: string;
    isSettingsSaved: boolean = false;
    formattingSettings: IFormattingListSettings | null;
    labels: string[];
    items: string[];
    formattingCollection: any;
    chosenFormattingSetting: any;
    formattingName: string;
    showAlert:boolean;
    alertMessage:string;

    constructor(public _i18n: I18nService,
                private _router: Router,
                private _fileService: FileService,
                private _formattingListSettingService: FormattingListSettingsService) {
        this.title = this._i18n.translate('ATTACHMENT_SETTINGS_TITLE');
        this.showAlert = false;
    }

    ngOnInit() {
        this.title = this._i18n.translate('FORMATTING_SETTINGS');
        this.formattingSettings = Object.assign({}, this._formattingListSettingService.getFormattingListSettings());
        this.formattingCollection = [...this._formattingListSettingService.getFormattingListSettingsCollection(), defaultFormattingListSettings];
        this.items = Object.keys(this.formattingSettings);
        this.formattingName = this.formattingSettings.name;
        this.labels = [
            'LABEL_BEFORE_DESC',
            'LABEL_ATTACHMENT_NUMBER',
            'LABEL_AFTER_DESC'
        ];

    }

    onClose(): void {
        this.showAlert = false;
    }

    choseFormatting(formatting: IFormattingListSettings) {
        this.formattingSettings = JSON.parse(JSON.stringify(formatting));
        this.formattingName = this.formattingSettings.name;
    }

    deleteFormatting() {
        this.showAlert = true;
        this.alertMessage = 'Formatting deleted';
        this._formattingListSettingService.deleteFormattingFromFormattingListSettingsCollection(this.formattingSettings);
        this.formattingCollection = [...this._formattingListSettingService.getFormattingListSettingsCollection(), defaultFormattingListSettings];
        if (this.formattingSettings.name === this._formattingListSettingService.getFormattingListSettings().name) {
            this.formattingSettings = JSON.parse(JSON.stringify(defaultFormattingListSettings));
            this.formattingName = this.formattingSettings.name;
            this._formattingListSettingService.saveFormattingListSettings(this.formattingSettings).then(() => {
                this.isSettingsSaved = true;
            });
        } else {
            this.formattingSettings = JSON.parse(JSON.stringify(defaultFormattingListSettings));
            this.formattingName = this.formattingSettings.name;
        }
    }

    saveSettings() {
        this.showAlert = true;
        this.alertMessage = 'Formatting saved';
        this.formattingSettings.name = this.formattingName;
        this._formattingListSettingService.saveFormattingListSettings(this.formattingSettings).then(() => {
            this.isSettingsSaved = true;
        });
        this._formattingListSettingService.saveFormattingListSettingsToCollection(this.formattingSettings);
        this.formattingCollection = [...this._formattingListSettingService.getFormattingListSettingsCollection(), defaultFormattingListSettings];
    }

    changeSettings(settings: IFormattingListSettings) {
        console.log('settings', settings);
        this.formattingSettings = settings;
    }

    downloadSettings() {
        this._fileService.downloadFileFromSettings(this._formattingListSettingService.getFormattingListSettings());
    }


}