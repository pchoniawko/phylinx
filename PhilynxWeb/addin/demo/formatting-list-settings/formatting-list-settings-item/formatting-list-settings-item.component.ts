import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { I18nService } from '../../../common/i18n/i18n.service';
import { IFormattingListSettings } from '../IFormattingListSettings';

@Component({
    moduleId: module.id,
    selector: 'formatting-list-settings-item',
    templateUrl: './formatting-list-settings-item.component.html',
    styleUrls: [ './formatting-list-settings-item.component.css' ],
})
export class FormattingListSettingsItemComponent implements OnInit {
    @Input() label: string;
    @Input() item: string;
    @Input() formattingSettings: IFormattingListSettings;
    @Output() changeSetting = new EventEmitter();

    title: string;
    isSettingsSaved: boolean = false;
    constructor(private _i18n: I18nService) { }

    ngOnInit() {
        this.title = this._i18n.translate('FORMATTING_SETTINGS');
    }

    changeFormattingSettings(formattingSettings: IFormattingListSettings): void {
        this.changeSetting.emit({
            value: formattingSettings
        });
    }
}
