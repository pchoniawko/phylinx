import { Injectable } from '@angular/core'; 
import { loadState } from '../../common/loadState';
import { I18nService } from '../../common/i18n/i18n.service';
import { DateService } from './date.service';


@Injectable() 
export class TimelineService {

    data: any[];
    heading: string;
    
    constructor(private _i18n: I18nService,
                private _date: DateService
    ) {
        this.data =  loadState('timeline') || [];
    }

    getData() {
        return this.data;
    }
    
    getSingleEvent(index: number) {
        return this.data[index];
    }
    
    moveEventUp(index: number) {
         if (index !== 0 ) {
            this.data = this.data.map((el, i) => {
                let temp:any = this.data[index-1];
                if (i === index-1) {
                    return this.data[index];
                }
            
                else if (i === index) {
                    return temp;
                }
            
                else {
                    return el;
                }
            });
        }
        Office.context.document.settings.set('timeline', JSON.stringify(this.data));
        localStorage.setItem("timeline", JSON.stringify(this.data))
        return this.data;
    }
    
    moveEventDown(index: number) {
        if (index !== this.data.length-1) {
            this.data = this.data.map((el, i)=>{
                let temp: any = this.data[index];
                if (i === index) {
                    return this.data[index+1];
                }
                
                else if (i === index+1) {
                    return temp;
                }
                else {
                    return el;
                }
            });
        }
        Office.context.document.settings.set('timeline', JSON.stringify(this.data));
        localStorage.setItem("timeline", JSON.stringify(this.data))
        return this.data;
    }
    
    deleteEvent(index: number) {
         this.data.splice(index, 1);
         Office.context.document.settings.set('timeline', JSON.stringify(this.data));
         localStorage.setItem("timeline", JSON.stringify(this.data))
    }
    
    addNewEvent(date: String, summary: string) {
        console.log(this.data);
        this.data.push({date, summary});
        Office.context.document.settings.set('timeline', JSON.stringify(this.data));
        localStorage.setItem("timeline", JSON.stringify(this.data));
    }
    
    editEvent(index: number, date: string, summary: string) {
        this.data[index] = {date, summary};
        Office.context.document.settings.set('timeline', JSON.stringify(this.data));
        localStorage.setItem("timeline", JSON.stringify(this.data));
    }
    
    createEvent(input: string) {
        let event = this._date.extractDate(input);
        this.data.push(event);
        Office.context.document.settings.set('timeline', JSON.stringify(this.data));
        localStorage.setItem("timeline", JSON.stringify(this.data));
    }
    
}