import { Injectable } from '@angular/core'; 

export class DateService {
    
    constructor() {
    
    }
    
    extractDate(input: string) {
        
       
        
        let regEx =  /\d\d\d\d\s*[./]\s*\d(\d)?\s*[./]\s*(\d)?\d/;
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        regEx = /(\d)?\d\s*[./]\s*(\d)?\d\s*[./]\s*(\d\d\d\d)?/g;
        
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx =  /\d\d\d\d\s*([-]|[–])\s*(\d)?\d\s*([-]|[–])\s*(\d)?\d/;
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx = /(\d)?\d\s*([-]|[–])\s*(\d)?\d\s*([-]|[–])\s*\d\d\d\d/;
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx = /(\d)?\d\s*.\s*(I|II|III|IV|V|VI|VII|VIII|IX|X|XI|XII)(\s*.\s*\d\d\d\d)/;
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx = /\d\d\d\d\s*.\s*(I|II|III|IV|V|VI|VII|VIII|IX|X|XI|XII)\s*.\s*(\d)?\d/g;
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx = /(\d)?\d\s*.\s*(III|II|IV|VIII|VII|VI|IX|XII|XI|X|I|V)/;
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx = /\d\d\d\d\s*.\s*(stycz\w*|styczeń|lut\w*|mar\w*|kwiecień|kwie\w*|maj\w*|czerw\w*|lip\w*|sierpień|sierpni\w*|wrzesień||wrze\w*listopad\w*|paździer\w*|grudzień|grud\w*).(\d)?\d/i;
         if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx = /(\d)?\d\s*.\s*(styczeń|stycz\w*|lut\w*|mar\w*|kwiecień|kwie\w*|maj\w*|czerw\w*|lip\w*|sierpień|sierp\w*|wrziesień|wrze\w*|listopad\w*|paździer\w*|grudzień|grud\w*)(\s*.\s*\d\d\d\d)?/gi;
        if (input.match(regEx)) {
             let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx = /\d\d\d\d\s*.\s*(january|febuary|march|april|may|june|july|august|september|october|november|december)\s*.\s*(\d)?\d/i;
        console.log(input.match(regEx));
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx = /(\d)?\d\s*.\s*(january|febuary|march|april|may|june|july|august|september|october|november|december)(\s*.\s*\d\d\d\d)?/gi;
        console.log(input.match(regEx));
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
         regEx =/(january|febuary|march|april|may|june|july|august|september|october|november|december)\s*.\s*(\d)?\d(\s*.\s*\d\d\d\d)?/gi;
        console.log(input.match(regEx));
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
         regEx =/(january|febuary|march|april|may|june|july|august|september|october|november|december).*(\d)?\d\.*\d\d\d\d/gi;
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx = /\d\d\d\d\s*.\s*(jan|feb|mar|apr|may|jun|jul|aug|sep|sept|oct|nov|dec)\s*.\s*(\d)?\d/i;
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx = /(\d)?\d\s*.\s*(jan|feb|mar|apr|may|jun|jul|aug|sep|sept|oct|nov|dec)(\s*.\s*\d\d\d\d)/gi;
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx =/(jan|feb|mar|apr|may|jun|jul|aug|sept|sep|oct|nov|dec).*(\d)?\d.*\s*\d\d\d\d/gi;
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        regEx =/(jan|feb|mar|apr|may|jun|jul|aug|sept|sep|oct|nov|dec)\s*.\s*(\d)?\d(\s*.\s*\d\d\d\d)?/gi;
        if (input.match(regEx)) {
            let date = input.match(regEx)[0];
            let summary = input.replace(date, "");
            return {date, summary};
        }
        
        
        return {date: "00/00/0000", summary: input};
    }
}