import { Component, OnInit } from '@angular/core';
import { I18nService } from '../../common/i18n/i18n.service';
import { TimelineService } from './timeline-overview.service';

@Component({
    moduleId: module.id,
    selector: 'formatting-overview',
    templateUrl: './timeline-overview.component.html',
    styleUrls: [ './timeline-overview.component.css' ]
})
export class TimelineOverview implements OnInit {
 
   events: any[];
   assignedText: string;
   timelineOption: string;
   showLightbox: boolean;
   eventIndex: number;

  constructor(private _i18n: I18nService,
              private _timelineService: TimelineService
  ) {
        this.events = this._timelineService.getData();
        this.timelineOption = "table";
        this.showLightbox = false;
        this.assignedText = "";
 }

    ngOnInit() { }
    
    deleteEvent(index: number) {
       this._timelineService.deleteEvent(index);
    }
    
    moveUp(index: number) {
       this.events = this._timelineService.moveEventUp(index);
    }
    
    moveDown(index: number) {
        this.events = this._timelineService.moveEventDown(index);
    }
    
    addNewEvent() {
        this.eventIndex = -1;
        this.showLightbox = true;
    }
    
    editEvent(index: number) {
        this.eventIndex = index;
        this.showLightbox = true;
    }
    
    hideLightbox(event: any) {
        this.showLightbox = false;
    }
    
    generateTimeline() {
        console.log(this.events, this.timelineOption);
    }
    
    assignToTimeline() {
          Word.run((context: Word.RequestContext) => {
            const range = context.document.getSelection();
            context.load(range, 'text');
            return context.sync().then(() => {
                if(range.text.trim()){
                    this.assignedText += range.text.trim()+' ';
                    
                    
                }
            });

        })
    
    }
    
    createEvent() {
        this._timelineService.createEvent(this.assignedText);
        this.assignedText = "";
    }
}