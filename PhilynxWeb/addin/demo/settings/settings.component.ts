import { Component, OnInit } from '@angular/core';
import { I18nService } from '../../common/i18n/i18n.service';
import { ILinkDetail } from '../overview/link-detail.interface';

import { Router } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'settings',
    templateUrl: './settings.component.html',
    styleUrls: [ './settings.component.css' ]
})

export class Settings implements OnInit {
   details: ILinkDetail[];

  constructor(private _i18n: I18nService) {
    this.details = [
      { route: '/formatting-overview', title: this._i18n.translate('FORMATTING_OVERVIEW')}
    ];
  }

    ngOnInit() { }


}