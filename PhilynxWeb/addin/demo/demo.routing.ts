import {
  Routes,
  RouterModule
}  from '@angular/router';

import { OverviewComponent } from './overview/overview.component';
import { OfficeInteractionComponent } from './office-interaction/office-interaction.component';
import { FormattingSettingsComponent } from './formatting-settings/formatting-settings.component';
import { FormattingListSettingsComponent } from './formatting-list-settings/formatting-list-settings.component';
import { GenerateDocumentComponent } from './generate-document/generate-document.component';
import { SelectFormattingComponent } from './select-formatting/select-formatting.component';
import { SelectListFormattingComponent } from './select-list-formatting/select-list-formatting.component';
import { FormattingOverview } from './formatting-overview/formatting-overview.component';
import { AttachmentSettingsOverview } from './attachment-settings-overview/attachment-settings-overview.component';
import { ListSettingsOverview } from './list-settings-overview/list-settings-overview.component';
import { Settings } from './settings/settings.component';
import { SummaryComponent } from './summary/summary.component';
import { ListSettingsComponent } from './list-settings/list-settings.component';
import { TimelineComponent } from './timeline/timeline.component';
import { TimelineOverview } from './timeline-overview/timeline-overview.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'overview',
    pathMatch: 'full'
  },
  {
    path: 'overview',
    component: OverviewComponent
  },
  {
    path: 'office',
    component: OfficeInteractionComponent
  },
  {
    path: 'formatting-settings',
    component: FormattingSettingsComponent
  }
  ,
  {
    path: 'formatting-list-settings',
    component: FormattingListSettingsComponent
  },
  {
    path: 'generate-document',
    component: GenerateDocumentComponent
  },
  {
    path: 'select-formatting',
    component: SelectFormattingComponent
  },
  {
    path: 'select-list-formatting',
    component: SelectListFormattingComponent
  },
  {
    path: 'formatting-overview',
    component: FormattingOverview
  },
  {
    path: 'settings-overview',
    component: Settings
  },
  {
    path: 'attachment-settings-overview',
    component: AttachmentSettingsOverview
  },
  {
    path: 'list-settings-overview',
    component: ListSettingsOverview
  },
  
  {
    path: 'summary',
    component: SummaryComponent
    
  },
  {
    path: 'list-settings',
    component: ListSettingsComponent
  },
  
  { 
    path: 'timeline',
    component: TimelineComponent
  },
  
  {
    path: 'timeline-overview',
    component: TimelineOverview
  }
];

export const demoRouting = RouterModule.forRoot(routes);