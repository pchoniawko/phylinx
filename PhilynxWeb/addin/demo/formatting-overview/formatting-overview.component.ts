import { Component, OnInit } from '@angular/core';
import { I18nService } from '../../common/i18n/i18n.service';
import { ILinkDetail } from '../overview/link-detail.interface';
import {DocumentSettingsService} from '../../common/services/document-settings.service';
import {WordService} from '../../common/services/word.service';
import {loadStateString} from '../../common/loadState'


@Component({
    moduleId: module.id,
    selector: 'formatting-overview',
    templateUrl: './formatting-overview.component.html',
    styleUrls: [ './formatting-overview.component.css' ]
})
export class FormattingOverview implements OnInit {
   details: ILinkDetail[];
    startAttNumber:any;
  constructor(private _i18n: I18nService,
              private DocumentSettingsService:DocumentSettingsService,
              private WordService:WordService) {

      this.startAttNumber=loadStateString("startAttNumber");
      this.details = [
      { route: '/attachment-settings-overview',   title: 'Attachment settings' },
      { route: '/list-settings-overview',   title: 'List settings' },
    ];
  }

    ngOnInit() { }

    saveStartAttNumber(){
        localStorage.setItem("startAttNumber",this.startAttNumber);
        Office.context.document.settings.set("startAttNumber", this.startAttNumber);
        this.DocumentSettingsService.persistDocumentSettings();
        this.WordService.checkAttNumber();
    }

}
