import { Component, OnInit } from '@angular/core';
import { OfficeInteractionService } from '../office-interaction/office-interaction.service';

@Component({
    moduleId: module.id,
    selector: 'clear-office-settings',
    templateUrl: './clear-office-settings.component.html',
    styleUrls: ['./clear-office-settings.component.css']
})
export class ClearOfficeSettingsComponent implements OnInit {
    constructor(private _officeInteractionService: OfficeInteractionService) { }

    ngOnInit() { }

    clearOfficeSettings(): void {
        this._officeInteractionService.clearOfficeSettings();
    }
}
