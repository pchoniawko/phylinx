import {Component, OnInit} from '@angular/core';
import {DocumentSettingsService} from '../../common/services/document-settings.service';
import {loadStateString} from '../../common/loadState'

@Component({
    moduleId: module.id,
    selector: 'summary',
    templateUrl: './summary.component.html',
    styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

    disabled: boolean;
    summaryText: string;
    typingTimer: any;

    constructor(private _DocumentSettingsService: DocumentSettingsService,) {

        this.summaryText = this.summaryText = localStorage.getItem('summary');
        if (this.summaryText === null) {
            this.summaryText = Office.context.document.settings.get('summary');
            if (this.summaryText === null) {
                this.summaryText = '';
            }
        }
        const loggedDocument = loadStateString("logged");
        const documentName = loadStateString("documentName");

        if (loggedDocument !== documentName) {
            console.log("nadaje Dokumentowi nazwe",loggedDocument);
            localStorage.setItem("documentName", loggedDocument);
            Office.context.document.settings.set("documentName", loggedDocument);
            this._DocumentSettingsService.persistDocumentSettings();

            this.summaryText = '';
        }


    }

    ngOnInit() {
    }

    summaryChange(val: any) {
        const doneTypingInterval = 2500;
        const doneTyping = () => {
            document.getElementById("summaryTextArea").blur();
            console.log("unfocus!");
        };
        clearTimeout(this.typingTimer);
        this.typingTimer = setTimeout(doneTyping, doneTypingInterval);
        this.summaryText = val;
        Office.context.document.settings.set('summary', this.summaryText);
        localStorage.setItem('summary', this.summaryText);
        this._DocumentSettingsService.persistDocumentSettings();

    }

    addToSummary() {
        Word.run((context: Word.RequestContext) => {
            const range = context.document.getSelection();
            context.load(range, 'text');
            return context.sync().then(() => {
                if(range.text.trim()){
                    this.summaryText += range.text.trim() + '\n';
                    Office.context.document.settings.set('summary', this.summaryText);
                    localStorage.setItem('summary', this.summaryText);
                    this._DocumentSettingsService.persistDocumentSettings();
                }
            });
        })
    }

    insertSummary():void {
        Word.run((context) => {
            const range = context.document.getSelection();
            range.insertText(this.summaryText, Word.InsertLocation.after);
            return context.sync().then(() => {
            });
        }).catch((error)=> {
            console.log('Error: ' + JSON.stringify(error));
            if (error instanceof OfficeExtension.Error) {
                console.log('Debug info: ' + JSON.stringify(error.debugInfo));
            }
        });
    }
}