import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TimelineService } from '../timeline-overview/timeline-overview.service';
import { I18nService } from '../../common/i18n/i18n.service';

@Component({
    moduleId: module.id,
    selector: 'timeline-input',
    templateUrl: './timeline-input.component.html',
    styleUrls: ['./timeline-input.component.css']
})
export class TimelineInput implements OnInit {
    
    date: string;
    event: string;
   
    heading: string;
    @Input() eventIndex: number;
    
    @Output() close = new EventEmitter();

     constructor(private _timelineService: TimelineService,
                 private _i18n: I18nService
     ) {
        this.date = "";
        this.event = "";
    }

    ngOnInit() {
        if(this.eventIndex === -1 ) {
            this.heading = this._i18n.translate("CREATE_EVENT");
        }
        
        else if (this.eventIndex >= -1) {
            this.heading = this._i18n.translate("EDIT_EVENT");
            let eventToEdit = this._timelineService.getSingleEvent(this.eventIndex);
            this.date  = eventToEdit.date;
            this.event = eventToEdit.summary;
        }
    }
    
    closeLightbox() {
        console.log("close");
        this.close.emit({updated: false});
    }
    
    updateTimeline() {
        console.log(this.eventIndex);
        if (this.eventIndex === -1) {
            this._timelineService.addNewEvent(this.date, this.event);
            this.close.emit();
        }
        
        else if (this.eventIndex > -1 ) {
            this._timelineService.editEvent(this.eventIndex, this.date, this.event)
            this.close.emit();
        }
    }
        
}