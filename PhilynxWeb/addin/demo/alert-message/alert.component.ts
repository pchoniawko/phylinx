import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'alert-message',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.css']
})

export class AlertMessage implements OnInit {
    @Input() alertMessage: string;
    @Input() showMessage: boolean;
    @Output() close = new EventEmitter();
    
    ngOnInit() {
        
    }
    
    closeAlert() {
        this.showMessage = false;
        this.close.emit();
    }
}