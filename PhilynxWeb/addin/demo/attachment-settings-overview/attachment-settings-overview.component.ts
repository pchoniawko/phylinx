import { Component, OnInit } from '@angular/core';
import { I18nService } from '../../common/i18n/i18n.service';
import { ILinkDetail } from '../overview/link-detail.interface';
import {DocumentSettingsService} from '../../common/services/document-settings.service';


@Component({
    moduleId: module.id,
    selector: 'attachment-settings-overview',
    templateUrl: './attachment-settings-overview.component.html',
    styleUrls: [ './attachment-settings-overview.component.css' ]
})
export class AttachmentSettingsOverview implements OnInit {
   details: ILinkDetail[];
    startAttNumber:any;
  constructor(private _i18n: I18nService,
              private DocumentSettingsService:DocumentSettingsService) {

      this.details = [
      { route: '/select-formatting',   title: this._i18n.translate('SELECT_FORMATTING') },
      { route: '/formatting-settings', title: this._i18n.translate('CREATE_FORMATTING') },
    ];
  }
    ngOnInit() { }

}
