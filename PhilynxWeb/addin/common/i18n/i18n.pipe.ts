import { Injectable, Pipe } from "@angular/core";
import { I18nService } from "./i18n.service";
@Pipe({
    name: 'i18n'
})
export class I18nPipe {
    constructor(private i18n: I18nService) {
    }

    transform(key: string) {
        return this.i18n.translate(key);
    }
}