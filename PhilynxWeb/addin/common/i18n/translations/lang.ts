export const lang = {
    defaultLang: 'en',
        lang: {
            'ATTACHMENTS_PANEL': {
                            en: 'Attachments Panel',            
                            pl: 'Panel załączników'
                        },
            'ASSIGN_DESCRIPTION': {
                            en: 'Select text from document and click "Assign"',           
                            pl: 'Zaznacz tekst w dokumencie i naciśnij przypisz'
                        }, 
                      'OVERVIEW': {
                            en: 'Overview',
                            pl: 'Przegląd'
                        },
                 'START_PHILYNX': {
                           en: 'This is start project for Philynx',
                           pl: 'Strona startowa Philynx'
                       },
              'DISCOVER_PHILYNX': {
                           en: "Discover Philynx",
                           pl: "Odkryj Philynx"
                       },
          'FORMATTING_SETTINGS': {
                           en: 'Formatting settings',
                           pl: 'Ustwienia Formatowanie'
                       },
         'ATTACHMENT_SETTINGS': {
                           en: 'Attachment settings',
                           pl: 'Ustwienia załączników'
                       },
           'GENERATE_DOCUMENT': {
                            en: 'Generate document',
                            pl: 'Generuj dokument'
                       },
          'LABEL_BEFORE_DESC': {
                           en: 'Text before attachment name',
                           pl: 'Tekst przed nazwą załącznika'
                       },
      'LABEL_ATTACHMENT_DESC': {
                           en: 'Attachment description',
                           pl: 'Opis załącznika'
                       },
    'LABEL_ATTACHMENT_NUMBER': {
                           en: 'Attachment number description',
                           pl: 'Opis numeru załącznika'
                       },
          'LABEL_AFTER_DESC': {
                           en: 'Text after attachment name',
                           pl: 'Tekst po nazwie załącznika'
                       },
          'SAVE_FORMATTINGS_SETTINGS': {
                          en: 'Save settings',
                          pl: 'Zapisz ustawienia'
                       },
          'DOWNLOAD_FORMATTINGS_SETTINGS': {
                          en: 'Download settings',
                          pl: 'Pobierz ustawienia'
                       },
          'SETTINGS_FILE_NAME': {
                          en: 'settings.json',
                          pl: 'ustawienia.json'
                       },
   'ATTACHMENT_SETTINGS_TITLE': {
                          en: 'Save attachment settings',
                          pl: 'Ustawienia zapisu załączników'
                       },
      'ATTACHMENT_IN_DOCUMENT': {
                           en: 'Attachments in document',
                           pl: 'Załączniki w dokumencie'
                       },
     'ATTACHMENT_OUT_DOCUMENT': {
                           en: 'Attachments outside document',
                           pl: 'Załączniki poza dokumentem'
                       },
                'EMPTY_SELECT': {
                            en: 'Empty select',
                            pl: 'Brak treśći'
                       },
                'GENERATE_DOC': {
                            en: 'Generate common document',
                            pl: 'Utwórz wspólny dokument'
                       },
            'SELECT_FORMATTING': {
                            en: 'Select formatting',
                            pl: 'Wybierz formatowanie'
            },
            
            'USE_FORMATTING': {
                en: 'Use these formatting settings',
                pl: 'Używaj tego formatowania'
            },
            
            'CHANGE_FORMATTING': {
                en: 'The formatting settings were changed',
                pl: 'Zmieniono ustawienia formatowania'
            },
            
            'NEW_STYLE': {
                en: 'New Style',
                pl: 'Nowy styl'
            },
            
            'ASSIGN': {
                en: 'Assign text to attachment',
                pl: 'Oznacz tekst jako załącznik'
            },
            
            'LIST_OF_ATTACHMENTS': {
                en: 'Insert list of attachments',
                pl: 'Wstaw listę załączników'
            },
            
            'GEN_PDF': {
                en: 'Generate PDF',
                pl: 'Utwórz PDF'
            },
            
            'ATTACHMENTS': {
                en: 'Attachments',
                pl: 'Załączniki'
            },
            
            'FORMATTING_OVERVIEW': {
                en: 'Formatting settings',
                pl: 'Ustawienia formatowania'
            },
            
            'CREATE_FORMATTING': {
                en: 'Create new formatting',
                pl: 'Utwórz nowe formatowanie'
            },
            
            'SETTINGS': {
                en: 'Settings',
                pl: 'Ustawienia'
            },
            
            'ADD_TO_SUMMARY': {
                en: ' Add selected text to summary',
                pl: 'Dodaj zaznaczony tekst do podsumowania'
            },
            
            'DOCUMENT_NAME': {
                en: 'Document name',
                pl: 'Nazwa dokumentu'
            }, 
            
            'PACKAGE_SIZE': {
                en: 'Size of packages',
                pl: 'Rozmiar paczek'
            },
            
            'CREATE_PROJECT': {
                en: 'Create new project',
                pl: 'Utwórz nowy projekt'
            },
            
            'TIMELINE' : {
                en: 'Timeline',
                pl: 'Linia czasu'
            },
            'SAVE_BUTTON': {
                en: 'Save',
                pl: 'Zapisz'
            }, 
            'ADD_EVENT': {
                en: 'Add an event',
                pl: 'Dodaj zdarzenie'
            },
            
            'GENERATE_TIMELINE': {
                en: 'Generate timeline',
                pl: 'Generuj linię czasu'
            },
            
            'ADD_NEW_EVENT': {
                en: 'Add an event',
                pl: 'Dodaj zdarzenie'
            },
            
            'CURRENT_TIMELINE': {
                en: 'Current timeline',
                pl: 'Obecna linia'
            },
            
            'TABLE': {
                en: 'Table',
                pl: 'Tablica'
            },
            
            'LINE': {
                en: 'Graphic line',
                pl: 'Graficza linia'
            },
            
            'CREATE_EVENT': {
                en: 'Create new event',
                pl: 'Stwórz zdarzenie'
            },
            
            'EDIT_EVENT': {
                en: 'Edit the event',
                pl: 'Edytuj zdarzenie'
            },
            
            'CANCEL': {
                en: 'Cancel',
                pl: 'Anuluj'
            },
            
            'NO_EVENTS': {
                en: 'The timeline is empty. Add an event using the form above or the button below.',
                pl: 'Linia czasu jest pusta. Dodaj zdarzenie za pomocą formularza powyżej lub przycisku poniżej.'
            },
            
            'DATE': {
                en: 'Date',
                pl: 'Data'
            },
            
            'EVENT': {
                en: 'Event',
                pl: 'Zdarzenie'
            },
            'CLEAR_SETTINGS': {
                en: 'Clear Office settings',
                pl: 'Wyczyść ustawienia pamięci'
            }
        }    
};
