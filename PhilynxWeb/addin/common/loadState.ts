export const loadState = (key: string) => {
    try {
        if (localStorage) {
            if (localStorage.getItem(key) === null) {
                return undefined;
            }
            return JSON.parse(localStorage.getItem(key));
        } else {
            if (Office.context.document.settings.get(key) === null) {
                return undefined;
            }
            return JSON.parse(Office.context.document.settings.get(key));
        }

    } catch (err) {
        return undefined;
    }
};

export const loadStateString = (key: string) => {
    try {
        if (localStorage.getItem(key) === null) {
            if (Office.context.document.settings.get(key) === null) {
                return undefined;
            }
            return Office.context.document.settings.get(key);
        }
        return localStorage.getItem(key);
    } catch (err) {
        return undefined;
    }
};

