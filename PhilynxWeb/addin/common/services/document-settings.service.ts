﻿import { Injectable } from '@angular/core';
import { ContainerService } from './container.service';

@Injectable()
export class DocumentSettingsService {

    constructor(private container: ContainerService) { }

    private maxFileSize: number = 1024 * 1024 * 10; // MB
    private maxPartFileSize: number = 1024 * 1024 * 2;

    setDocumentSettings(attachmentId: string, value: string): boolean {
        const partsCount: number = Math.ceil(value.length / this.maxPartFileSize);

        // validate
        if (value.length >= this.maxFileSize) {
            this.container.addDataLogger(
                `Too big file ('${Math.round(value.length / (1024 * 1024) * 10) / 10} MB)! 
                Max size: ${this.maxFileSize / (1024 * 1024)} MB`
                );
            return false;
        }

        for (let i = 0; i < partsCount; i++) {
            var part = value.substr(i * this.maxPartFileSize, this.maxPartFileSize);
            Office.context.document.settings.set(`attachment-${attachmentId}-${i}`, part);
            if(localStorage){
                localStorage.setItem(`attachment-${attachmentId}-${i}`, part);
            }
        }
        return true;
    }

    getDocumentSettings(attachmentId: string): string {
        let i = 0;
        let part: string = '';
        let result: string = '';
        if(localStorage.getItem(`attachment-${attachmentId}-${i}`)){
            while (part = localStorage.getItem(`attachment-${attachmentId}-${i}`)) {
                i++;
                result += part;
            }
        }
        while (part = Office.context.document.settings.get(`attachment-${attachmentId}-${i}`)) {
            i++;
            result += part;
        }
        return result;
    }

    removeDocumentSettings(attachmentId: string): void {
        var i = 0;
        if(localStorage.getItem(`attachment-${attachmentId}-${i}`)) {
            while (localStorage.getItem(`attachment-${attachmentId}-${i}`)) {
                localStorage.removeItem(`attachment-${attachmentId}-${i}`);
                i++;
            }
        }
            while (Office.context.document.settings.get(`attachment-${attachmentId}-${i}`)) {
                Office.context.document.settings.remove(`attachment-${attachmentId}-${i}`);
                i++;
            }
    }

    persistDocumentSettings(): void {
        Office.context.document.settings.saveAsync((asyncResult) => {
            if (asyncResult.status === Office.AsyncResultStatus.Failed) {
                this.container.addDataLogger(`Settings save failed. Error: ${asyncResult.error.message}`);
            }
            if (asyncResult.status === Office.AsyncResultStatus.Succeeded) {
                this.container.addDataLogger('Saved success.');
            }
        });
    }
}