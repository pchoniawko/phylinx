import { Injectable } from '@angular/core';
import { Attachment } from '../../common/models/attachment';
import { DocumentSettingsService } from './document-settings.service';
import { I18nService } from '../i18n/i18n.service';

@Injectable()
export class FileService {

    constructor(private documentSettingsService: DocumentSettingsService,
        private _i18n: I18nService) { }

    saveFileToAttachment(file: Blob, attachment: Attachment) {
        const _this = this;
        const reader = new FileReader();

        reader.addEventListener("load", function () {
            attachment.fileBase64 = reader.result;
            _this.documentSettingsService.setDocumentSettings(attachment.id, attachment.toJSON());
            _this.documentSettingsService.persistDocumentSettings();
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }
    }

    downloadFileFromAttachment(attachment: Attachment) {
        let textFileAsBlob: any;
        if (Office.context.document.settings.get('attachmentSetting') === 'IN') {
             textFileAsBlob = this.dataUrlToBlob(attachment.fileBase64); 
        } else {
             textFileAsBlob = attachment.filePath; 
        }
        const fileName = attachment.fileName();
        this.downloader(textFileAsBlob, fileName);
    }

    downloadFileFromSettings(settings: any): Promise<Blob> {
        return new Promise((resolve, reject) => {
            const blob: Blob = new Blob([JSON.stringify(settings)], {type: 'aplication/json'});
            console.log("settings",settings);
            console.log("[JSON.stringify(settings)]",[JSON.stringify(settings)]);
            console.log("blob",blob);
            const fileName: string = this._i18n.translate('SETTINGS_FILE_NAME');
            this.downloader(blob, fileName);
            resolve(blob);
        });
    }

    downloader(blob: any, fileName: string) {
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveBlob(blob, fileName);

        } else {
            const objectUrl= URL.createObjectURL(blob);
            const a = window.document.createElement("a");
            a.href = objectUrl;
            a.download = fileName;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
            URL.revokeObjectURL(objectUrl);
        }
    }

    dataUrlToBlob(strUrl: string) {
        const parts = strUrl.split(/[:;,]/),
            type = parts[1],
            decoder = parts[2] === "base64" ? atob : decodeURIComponent,
            binData = decoder(parts.pop()),
            mx = binData.length,
            uiArr = new Uint8Array(mx);

        const binUi = uiArr.map((el, i) => binData.charCodeAt(i));

        return new Blob([binUi], { type: type });
    }
}