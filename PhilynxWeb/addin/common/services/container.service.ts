﻿import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Attachment } from '../models/attachment';

@Injectable()
export class ContainerService {
    private attachments: Array<Attachment>; 
    private loggerMessages = new Subject<string>();
    dataLogger$ = this.loggerMessages.asObservable();

    constructor() {
        this.attachments = new Array<Attachment>();
    }

    getAttachments = (): Array<Attachment>  => this.attachments;

    private static compare(a:Attachment, b:Attachment):number {
        const numberA = a.content.match(/\d+/g);
        const numberB = b.content.match(/\d+/g);
        if (numberA < numberB) {
            return -1;
        } else if (numberA > numberB) {
                return 1;
        } else {
            return 0;
        }
    }
    sortAttachments = () => this.attachments.sort(ContainerService.compare);

    findMaxId = (): number => Math.max.apply(Math, this.attachments.map((att)=>att.id));

    addAttachment = (attachment: Attachment): Attachment[] => {
        return this.attachments = this.attachments.every((att) => att.id !== attachment.id) ?
            [...this.attachments, attachment] :
            this.attachments;
    }

    removeAttachment = (attachment: Attachment): Attachment[] => {
        return this.attachments = this.attachments.filter((att) => att.id !== attachment.id);
    }

    updateAttachment = (attachment: Attachment): Attachment[] => {
        const updatedIndex = this.attachments.findIndex((att) => att.id === attachment.id);
        return this.attachments = [
            ...this.attachments.slice(0, updatedIndex),
            attachment,
            ...this.attachments.slice(updatedIndex + 1)
        ];
    }

    getAttachmentById = (id: string): Attachment => this.attachments.find((att) => att.id === id);

    addDataLogger = (value: string) => this.loggerMessages.next(value);

}