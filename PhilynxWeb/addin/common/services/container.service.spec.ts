import { ContainerService } from './container.service';
let service: ContainerService;
beforeEach(() => {
        service = new ContainerService();
    });

describe('Service: ContainerService', () => {
    test('addAttachment', () => {
        const newAttachment: any = { id: '3', content: "Attachment 3", filePath: '', fileBase64: '' };
        const expected: any[] = [{ id: '3', content: "Attachment 3", filePath: '', fileBase64: '' }];
        service.addAttachment(newAttachment);
        expect(service.getAttachments()).toEqual(expected);
    });

    test('removeAttachment', () => {
        const started: any[] = [];
        let attachment: any = { 
            id: (Math.floor((Math.random() * 1000) + 1000)).toString(), 
            content: "Attachment", 
            filePath: '', 
            fileBase64: '' 
        };
        started.push(attachment);
        service.addAttachment(attachment);
        attachment = { 
            id: (Math.floor((Math.random() * 1000) + 1000)).toString(), 
            content: "Attachment", 
            filePath: '', 
            fileBase64: '' 
        };
        started.push(attachment);
        service.addAttachment(attachment);

        const expected = service.getAttachments()[0];
        service.removeAttachment(expected);
        
        expect(service.getAttachments()).toEqual(started.slice(1));
    });

    test('updateAttachment', () => {
        const started: any[] = [];
        let attachment: any = { 
            id: (Math.floor((Math.random() * 1000) + 1000)).toString(), 
            content: "Attachment", 
            filePath: '', 
            fileBase64: '' 
        };
        started.push(attachment);
        service.addAttachment(attachment);
        attachment = { 
            id: (Math.floor((Math.random() * 1000) + 1000)).toString(), 
            content: "Attachment", 
            filePath: '', 
            fileBase64: '' 
        };
        started.push(attachment);
        service.addAttachment(attachment);
        
        const expected = service.getAttachments()[0];
        expected.content = "changed";
        service.updateAttachment(expected);
        started[0].content = expected.content;
        expect(service.getAttachments()).toEqual(started);
    });
});
