import {Injectable} from '@angular/core';
import {IOfficeResult} from '../../office/ioffice-result';
import {Attachment} from '../../common/models/attachment';
import {IBindingResult} from '../../office/ibinding-result';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {ContainerService} from './container.service';
import {Subject} from 'rxjs/Subject';
import {DocumentSettingsService} from './document-settings.service';
import {IFormattingSettings} from '../../demo/formatting-settings/IFormattingSettings';
import {I18nService} from '../i18n/i18n.service';
import {FormattingSettingsService} from '../../demo/formatting-settings/formatting-settings.service';
import {createHtml} from '../createHtml';
import {loadState, loadStateString} from '../loadState';


@Injectable()
export class WordService {
    private document: Office.Document = Office.context.document;
    htmlHelper: any;
    dataSource = new Subject<any>();
    data$ = this.dataSource.asObservable();
    attachmentsCounter: number = 0;

    constructor(private container: ContainerService,
                private documentSettingsService: DocumentSettingsService,
                private _i18n: I18nService,
                private _formattingSettingSevice: FormattingSettingsService) {
    }

    async setSelectedTextAsAttachment(): Promise<IOfficeResult> {
        return new Promise((resolve, reject) => {
            this.document.getSelectedDataAsync(Office.CoercionType.Text, (result: Office.AsyncResult) => {
                if (result.status === Office.AsyncResultStatus.Failed) {
                    reject({error: "Failed to retrieve data. Error: " + result.error.message});
                }
                else {
                    const contents = this.container.getAttachments().map((att: Attachment) => att.content);
                    const isInAttachment = contents.some((con) => con.includes(result.value));
                    if (isInAttachment) {
                        const existingAttachment = this.container.getAttachments().find((att) => att.content.includes(result.value));
                        // console.log('atts', this.container.getAttachments(), existingAttachment);
                        this.setBindingToSelection(parseInt(existingAttachment.id))
                            .then((result: IBindingResult) => {
                                const binding: Office.Binding = result.binding;
                                this.addListenerToBinding(binding);
                                resolve({value: existingAttachment});
                                return binding;
                            });
                    } else {
                        this.setBindingToSelection(this.attachmentsCounter)
                            .then((result: IBindingResult) => {
                                const binding: Office.Binding = result.binding;
                                binding.getDataAsync({coercionType: Office.CoercionType.Text}, (result: Office.AsyncResult) => {
                                    if (result.status === Office.AsyncResultStatus.Succeeded) {
                                        const attachment = new Attachment();
                                        const re = /-\d+/g;
                                        attachment.id = binding.id.replace(re, "");
                                        attachment.content = result.value;
                                        this.addListenerToBinding(binding);
                                        this.attachmentsCounter++;
                                        this.documentSettingsService.setDocumentSettings(attachment.id, attachment.toJSON());
                                        this.documentSettingsService.persistDocumentSettings();
                                        resolve({value: attachment});
                                    }
                                });
                                return binding;
                            });
                    }
                }
            });
        });
    }

    setBindingToSelection(id: number): Promise<IBindingResult> {
        const formattingSettings: IFormattingSettings = this._formattingSettingSevice.getFormattingSettings();
        return new Promise((resolve, reject) => {
            this.document.bindings.addFromSelectionAsync(
                Office.BindingType.Text,
                { id: id.toString()+'-'+this.generateGuid(), value: '' },
                (result) => {
                    if (result.status === Office.AsyncResultStatus.Failed) {
                        reject({ status: false });
                    } else {
                        Word.run((context: Word.RequestContext) => {
                            const contentControls = context.document.contentControls;
                            const range = context.document.getSelection();
                            context.load(range, 'text');
                            return context.sync().then(()=>{
                                const myContentControl = range.insertContentControl();

                                myContentControl.tag = `binding`;
                                myContentControl.insertHtml(
                                    createHtml(formattingSettings,range.text, 0).insert,
                                    'replace'
                                );

                                context.load(contentControls, 'id');

                                return context.sync();});

                        }).then(() => {
                            this.checkAttNumber();
                            resolve({ status: true, binding: result.value });
                        }).catch((error) => {
                            console.log(`Error: ${JSON.stringify(error)}`);
                            if (error instanceof OfficeExtension.Error) {
                                console.log(`Debug info: ${JSON.stringify(error.debugInfo)}`);
                            }
                        });
                    }
                });
        });
    }

    checkAttNumber(newFormattingSettings:any=''){
        const formattingSettings: IFormattingSettings = this._formattingSettingSevice.getFormattingSettings();
        return new Promise ((resolve,reject)=>{
            Word.run((context: any) => {

                const contentControlsBinding = context.document.contentControls.getByTag('binding');
                context.load(contentControlsBinding, ['text','tag']);

                return context.sync().then(() => {
                    const contentControlsItems = contentControlsBinding.items;

                    if (contentControlsItems.length === 0) {
                        console.log("There isn't a content control with a tag of binding in this document.");
                        resolve();
                    } else {
                        const regNumberVar  = formattingSettings.attNumber.value+" \\d+";
                        const regAttNumber = new RegExp(regNumberVar,'g');
                        const uniqueContent=contentControlsItems
                            .map((controller:any)=>{
                                return  controller.text
                                    .replace(formattingSettings.beforeDesc.value, "")
                                    .replace(formattingSettings.afterDesc.value, "")
                                    .replace(regAttNumber, "")
                                    .trim();
                            })
                            .filter((elem:any, index:any, self:any)=> index === self.indexOf(elem));

                        contentControlsItems.forEach((controller:any)=> {
                            const contentWithNumber = controller.text.match(regAttNumber);
                            const attNumber = Number(contentWithNumber[0].match(/\d+/g))-1;

                            const content = controller.text
                                .replace(formattingSettings.beforeDesc.value, "")
                                .replace(formattingSettings.afterDesc.value, "")
                                .replace(regAttNumber, "")
                                .trim();
                            const startAttNumber = loadStateString("startAttNumber")?Number(loadStateString("startAttNumber"))-1:0;
                            const id = uniqueContent.indexOf(content)+startAttNumber;
                            if(newFormattingSettings) {
                                controller.insertHtml(
                                    createHtml(newFormattingSettings, content, id).insert,
                                    'replace'
                                );
                            } else {
                                if(attNumber!==id){
                                    controller.insertHtml(
                                        createHtml(formattingSettings, content, id).insert,
                                        'replace'
                                    );
                                }
                            }
                        });
                        return context.sync().then(()=>{
                            resolve();
                        });
                    }

                });

            });
        });


    }



    getStoredAttachments(): Observable<Attachment> {

        this.attachmentsCounter = 0;
        return new Observable<Attachment>((observer: Observer<Attachment>) => {
            this.document.bindings.getAllAsync((resultAttachemts: Office.AsyncResult) => {
                if (resultAttachemts.status === Office.AsyncResultStatus.Succeeded) {
                    resultAttachemts.value.forEach((attBinding: Office.Binding) => {
                        const re = /\d+-/g;
                        const id = attBinding.id.match(re).toString().slice(0, -1);
                        const attachmentStr = this.documentSettingsService.getDocumentSettings(id);
                        if (attachmentStr) {
                            const attachment = new Attachment();
                            this.addListenerToBinding(attBinding);
                            attachment.fromJSON(attachmentStr);
                            this.attachmentsCounter++;
                            observer.next(attachment);
                        }
                    });
                }
            });
        });
    }

    highlightAttachment(attachment: Attachment): Promise<IBindingResult> {
        return new Promise((resolve, reject) => {
            this.getBindingForAttachment(attachment)
                .then((result: IBindingResult) => {
                    resolve(result);
                });
        });
    }

    getBindingForAttachment(attachment: Attachment): Promise<IBindingResult> {
        return new Promise((resolve, reject) => {
            Office.context.document.bindings.getByIdAsync(attachment.id, (result: Office.AsyncResult) => {
                if (result.status === Office.AsyncResultStatus.Succeeded) {
                    resolve({status: true, binding: result.value});
                } else {
                    resolve({status: false, binding: result.value});
                }
            });
        });
    }

    generateGuid = (): string => this.attachmentsCounter.toString();



    refreshBinding(binding: Office.Binding) {
        console.log("refreshed");
        binding.getDataAsync({coercionType: Office.CoercionType.Text}, (result: Office.AsyncResult) => {
            binding.setDataAsync(result.value, (result: Office.AsyncResult) => {
                    if (result.status === Office.AsyncResultStatus.Failed) {
                        console.log(result.error.message);
                    }
                }
            );
        });
    }


    deleteContentControl(attachment: Attachment): Promise<any> {
        return new Promise((resolve) => {
            Word.run((context: any) => {
                console.log("attachment.content", attachment.content);
                const contentControls = context.document.contentControls;

                context.load(contentControls, 'text');

                return context.sync().then(() => {
                    if (contentControls.items.length === 0) {
                        console.log("There isn't a content control with a tag of binding in this document.");
                    } else {
                        const formatingSettings: IFormattingSettings = this._formattingSettingSevice.getFormattingSettings();
                        contentControls.items.forEach((controller: any, index: any) => {
                            if (controller.text.trim() === attachment.content.trim()) {
                                const regNumberVar  = formatingSettings.attNumber.value+" \\d+";
                                const regAttNumber = new RegExp(regNumberVar,'g');

                                let newContent = attachment.content
                                    .replace(formatingSettings.beforeDesc.value, "")
                                    .replace(formatingSettings.afterDesc.value, "")
                                    .replace(regAttNumber, "")
                                    .trim();

                                console.log("Usunieto", controller, "o Indexie", index);
                                contentControls.items[index].insertText(newContent, 'Replace');
                                contentControls.items[index].delete(true);
                            }
                        });
                    }

                    return context.sync().then(() => {
                        console.log('The font has changed.');
                        resolve();
                    });
                });
            });

        });
    }


    removeBinding = (element: any): Promise<any> => {
        return new Promise((resolve, reject) => {
            Office.context.document.bindings.releaseByIdAsync(element.id, (result: Office.AsyncResult) => {
                if (result.status === Office.AsyncResultStatus.Succeeded) {
                    console.log("usunalem binding o id", element.id);
                    resolve({status: true, binding: result.value});
                }
            });
        });
    }


    removeBindingByAttachment = async (attachment: Attachment): Promise<any> => {
        return new Promise((resolve, reject) => {
            Office.context.document.bindings.getAllAsync((asyncResult: any) => {
                const match = attachment.id + "-";
                const re = new RegExp(match, "g");
                asyncResult.value
                    .filter((element: any) => element.id.search(re)!==-1)
                    .map(async (element: any) => {
                        await this.removeBinding(element);
                    });
                resolve();
            });
        });

    }

    updatesRelatedBindingsPrototype(att: Attachment){
            Office.context.document.bindings.getAllAsync((asyncResult: any) => {
                const match = att.id + "-";
                const re = new RegExp(match, "g");
                asyncResult.value
                    .filter((binding: any) => binding.id.search(re)!==-1)
                    .map((binding: any) => {
                        binding.setDataAsync(att.content, (result: Office.AsyncResult) => {
                                if (result.status === Office.AsyncResultStatus.Failed) {
                                    console.log(result.error.message);
                                }
                            }
                        );
                    });
            });
    }

    updatesRelatedBindings(att: Attachment) {
        return new Promise((resolve, reject) => {
            const formattingSettings: IFormattingSettings = this._formattingSettingSevice.getFormattingSettings();

            Word.run((context: any) => {
                const contentControlsBinding = context.document.contentControls.getByTag('binding');
                const contentControlsRelated = context.document.contentControls.getByTag('related');
                context.load(contentControlsBinding, ['text', 'tag']);
                context.load(contentControlsRelated, ['text', 'tag']);

                return context.sync().then(() => {
                    const contentControlsItems = contentControlsBinding.items.concat(contentControlsRelated.items);

                    if (contentControlsItems.length === 0) {
                        console.log("There isn't a content control with a tag of binding in this document.");
                    } else {
                        const re = /\d+/g;
                        const attContent = att.content
                            .replace(formattingSettings.beforeDesc.value, "")
                            .replace(formattingSettings.afterDesc.value, "")
                            .replace(formattingSettings.attNumber.value, "")
                            .replace(re, "");
                        contentControlsItems.forEach((controller: any, index: any) => {
                            const re = /\d+/g;
                            const content = controller.text
                                .replace(formattingSettings.beforeDesc.value, "")
                                .replace(formattingSettings.afterDesc.value, "")
                                .replace(formattingSettings.attNumber.value, "")
                                .replace(re, "");

                            if ((attContent.trim().includes(content.trim())
                                || content.trim().includes(attContent.trim()))
                                && (attContent !== content)) {
                                const re = /\d+/g;
                                const numberOfAttachment = Number(att.content.match(re)) - 1;

                                controller.insertHtml(
                                    createHtml(formattingSettings,attContent, numberOfAttachment).insert,
                                    'replace'
                                );
                            }
                        });
                    }
                    return context.sync().then(() => {
                        resolve();
                    });
                });

            });
        });

    }

    addListenerToBinding(binding: Office.Binding) {
        binding.addHandlerAsync(Office.EventType.BindingDataChanged, (eventArgs: any) => {

                const re = /-\d+/g;
                const id = eventArgs.binding.id
                    .replace(re, "");
                const att: Attachment = this.container.getAttachmentById(id);
                if (att) {
                    binding.getDataAsync({coercionType: Office.CoercionType.Text}, (result: Office.AsyncResult) => {
                        if (result.status === Office.AsyncResultStatus.Succeeded) {

                                att.content = result.value;
                                // this.updatesRelatedBindingsPrototype(att);
                                console.log("updatuje id");
                                this.container.addDataLogger('Updated binding');
                                this.dataSource.next(att);

                                this.documentSettingsService.setDocumentSettings(att.id, att.toJSON());
                                this.documentSettingsService.persistDocumentSettings();

                        }
                    });
                }
            },
            {asyncContext: binding},
            (AsyncResult: any) => this.refreshBinding(AsyncResult.asyncContext));
    }
}