import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'ui-toggle',
  templateUrl: './toogle.component.html',
  styleUrls: ['./toogle.component.css'],
  host: {
    class: 'toggle'
  }
})
export class ToogleComponent implements OnInit {

  @Input() optionsList: any;
  @Input() selectedOption: any;
  @Output() selectedOptionChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
    if (this.selectedOption === undefined && this.optionsList) {
      this.selectedOption = this.optionsList[0];
    }
  }
  onOptionActivate(option: any) {
    this.selectedOption = option;
    this.selectedOptionChange.next(option);
  }

}
