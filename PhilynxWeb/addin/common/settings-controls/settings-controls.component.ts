import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { SettingsControlsService } from './settings-controls.service';
import { IFormattingSettings } from '../../demo/formatting-settings/IFormattingSettings';

@Component({
    moduleId: module.id,
    selector: 'settings-controls',
    templateUrl: './settings-controls.component.html',
    styleUrls: ['./settings-controls.component.css']
})
export class SettingsControlsComponent implements OnInit {
    
    @Input() formattingSettings: IFormattingSettings;
    @Input() item: string;
    @Output() toggle = new EventEmitter();
    fontWeight: string;
    fontStyle: string;
    textDecoration: string;
    size: number;
    sizes: number[];
    fontFamily:string;
    fontFamilies:string[];
    constructor(private _settingsControlsService: SettingsControlsService) {
        this.sizes = [6, 6.5, 8, 9, 10, 10.5, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 38, 48, 72];
        this.fontFamilies =['Tahoma','Arial', 'Times','Times New Roman'];
     }
    
    ngOnInit() { }

    toggleFontWeight(): void {
        this.fontWeight === 'bold' ? this.fontWeight = 'normal' : this.fontWeight = 'bold';
        this.formattingSettings[this.item].fontWeight = this.fontWeight;
        this.toggle.emit({
            value: this.formattingSettings
        });
    }
    toggleFontStyle(): void {
        this.fontStyle === 'italic' ? this.fontStyle = 'normal' : this.fontStyle = 'italic';
        this.formattingSettings[this.item].fontStyle = this.fontStyle;
        this.toggle.emit({
            value: this.formattingSettings
        });
    }

    toggleTextDecoration(): void {
        this.textDecoration === 'underline' ? this.textDecoration = 'nome' : this.textDecoration = 'underline';
        this.formattingSettings[this.item].textDecoration = this.textDecoration;
        this.toggle.emit({
            value: this.formattingSettings
        });
    }

    changeSize(size: number): void {
        this.formattingSettings[this.item].fontSize = size;
        this.toggle.emit({
            value: this.formattingSettings
        });
    }

    changeFontFamily(fontFamily: string) {
        this.formattingSettings[this.item].fontFamily = fontFamily;
        this.toggle.emit({
            value: this.formattingSettings
        });
    }
}
