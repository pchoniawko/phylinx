import { IFormattingSettings } from '../demo/formatting-settings/IFormattingSettings';
import { IFormattingListSettings } from '../demo/formatting-list-settings/IFormattingListSettings';


export const createHtml = (formattingSettings: IFormattingSettings|IFormattingListSettings,content:string, attachmentsCounter: number,numberPosition:string='DEFAULT') => {
    switch(numberPosition){
        case 'BEFORE_DESC':
            return {insert:beforeDescFormatting(formattingSettings,content,attachmentsCounter)};
        case 'AFTER_DESC':
            return {insert:defaultFormatting(formattingSettings,content,attachmentsCounter)};
        case 'DEFAULT':
            return {insert:defaultFormatting(formattingSettings,content,attachmentsCounter)};
        default:
            return {insert:defaultFormatting(formattingSettings,content,attachmentsCounter)};
    }
};

const defaultFormatting=(formattingSettings: IFormattingSettings|IFormattingListSettings,content:string, attachmentsCounter: number)=>(`
        <span
            style="font-weight: ${formattingSettings.beforeDesc.fontWeight};
               font-style: ${formattingSettings.beforeDesc.fontStyle};
               font-family: ${formattingSettings.beforeDesc.fontFamily};
               font-size: ${formattingSettings.beforeDesc.fontSize}px">
                    ${formattingSettings.beforeDesc.value}
        </span>
   
 ${content }
 
        <span
            style="font-weight: ${formattingSettings.attNumber.fontWeight};
               font-style: ${formattingSettings.attNumber.fontStyle};
               font-family: ${formattingSettings.attNumber.fontFamily};
               font-size: ${formattingSettings.attNumber.fontSize}px">
                    ${formattingSettings.attNumber.value} ${attachmentsCounter + 1}
        </span>
        <span
            style="font-weight: ${formattingSettings.afterDesc.fontWeight};
           font-style: ${formattingSettings.afterDesc.fontStyle};
           font-family: ${formattingSettings.afterDesc.fontFamily};
           font-size: ${formattingSettings.afterDesc.fontSize}px">
                ${formattingSettings.afterDesc.value}
        </span>
    `);

const beforeDescFormatting =(formattingSettings: IFormattingSettings|IFormattingListSettings,content:string, attachmentsCounter: number)=>(`
        <span
            style="font-weight: ${formattingSettings.beforeDesc.fontWeight};
               font-style: ${formattingSettings.beforeDesc.fontStyle};
               font-family: ${formattingSettings.beforeDesc.fontFamily};
               font-size: ${formattingSettings.beforeDesc.fontSize}px">
                    ${formattingSettings.beforeDesc.value} ${attachmentsCounter + 1}:
        </span>
   
 ${content }
 
        <span
            style="font-weight: ${formattingSettings.attNumber.fontWeight};
               font-style: ${formattingSettings.attNumber.fontStyle};
               font-family: ${formattingSettings.attNumber.fontFamily};
               font-size: ${formattingSettings.attNumber.fontSize}px">
                    ${formattingSettings.attNumber.value} 
        </span>
        <span
            style="font-weight: ${formattingSettings.afterDesc.fontWeight};
           font-style: ${formattingSettings.afterDesc.fontStyle};
           font-family: ${formattingSettings.afterDesc.fontFamily};
           font-size: ${formattingSettings.afterDesc.fontSize}px">
                ${formattingSettings.afterDesc.value}
        </span>
    `);