import { loadState } from './loadState';
import { IFormattingSettings } from '../demo/formatting-settings/IFormattingSettings';
import {defaultFormattingSettings} from '../demo/formatting-settings/default-settings-controls';

const formattingSettings: IFormattingSettings = loadState("formattingSettings") ? 
    loadState("formattingSettings") : 
    defaultFormattingSettings;

const dict = {"á":"a", "ą":"a", "ł":"l","ć":"c","ó":"o","ś":"s","ę":"e",
            "Ą":"A", "Ł":"L", "Ć":"C","Ó":"O","Ś":"S","Ę":"E"};
export const filterCharacters = (word:string)=> word.replace(/[^\w ]/g, (char)=> dict[char] || char);

const convertName = (name: string): string => {


    const slash = new RegExp('/','g');
    const attNumber = new RegExp(`${formattingSettings.attNumber.value} \\d+`,'g');
    const anyChar = /[éèêëùüàâöïç\".'?!,@$#:;]/g;
    const pureName = name.replace(formattingSettings.beforeDesc.value, "")
                .replace(formattingSettings.afterDesc.value, "")
                .replace(attNumber, "")
                .replace(slash, "_")
                .replace(anyChar, "")
                .trim();
    return this.filterCharacters(pureName).trim()
};


export default convertName;