import convertName from './convertName';

test("should convert file name", () => {
    const before = "/### Dowód: Umowa z dnia 16/12/15 ###/";
    const after = convertName(before);
    const expected = " Umowa z dnia 16_12_15 ";
    expect(after).toEqual(expected);
});