﻿export class Attachment {
    id: string;
    content: string;
    filePath: string;
    fileBase64: string;
    serverId?: number;
    serverName?: string;

    constructor() {
        this.id =  this.content = this.filePath = this.fileBase64 = this.serverName = '';
    }

    toJSON() {
        return JSON.stringify({
            id: this.id,
            content: this.content,
            filePath: this.filePath,
            fileBase64: this.fileBase64,
            serverName: this.serverName
        });
    }

    fromJSON(text: string) {
        const obj = JSON.parse(text);

        this.id = this.content = this.filePath = '';

        if (obj) {
            if (obj.id !== undefined) {
                this.id = obj.id;
            }
            if (obj.content !== undefined) {
                this.content = obj.content;
            }
            if (obj.filePath !== undefined) {
                this.filePath = obj.filePath;
            }
            if (obj.fileBase64 !== undefined) {
                this.fileBase64 = obj.fileBase64;
            }
            if (obj.serverName !== undefined) {
                this.serverName = obj.serverName;
            }
        }
    }

    filePathShort(): string {
        return this.filePath.slice(-18);
    }

    fileName = (): string => this.filePath.replace(/^.*[\\\/]/, '');

}
