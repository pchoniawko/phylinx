import { Response } from '@angular/http';
import {loadStateString} from './loadState'
export const downloader = (res: Response | any, extension:string,name:string=""): void => {
        const blob = new Blob([res._body], {type: res.headers.get('content-type')});
        const objectUrl = URL.createObjectURL(blob);
        const a = window.document.createElement("a");
        const filename = name?name:loadStateString("logged") ? loadStateString("logged") : 'download';
        a.href = objectUrl;
        a.download = filename+extension;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
};
