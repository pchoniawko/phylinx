export interface IBindingResult {
  status: boolean;
  binding?: Office.Binding;
}