import { Attachment } from '../common/models/attachment';

export interface IOfficeResult {
  error?: string;
  success?: string;
  value?: Attachment;
  valueObject?: any;
}