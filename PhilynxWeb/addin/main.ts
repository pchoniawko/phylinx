﻿import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule }              from './app.module';

import { enableProdMode } from '@angular/core';

enableProdMode();

// use this to run in browser for dev
// platformBrowserDynamic().bootstrapModule(AppModule);

// bootstrap with Office.js for in Office
Office.initialize = () => {
  console.log('Office init: bootstrapping Angular2');

  return platformBrowserDynamic().bootstrapModule(AppModule);
};