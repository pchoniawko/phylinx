import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserXhr, HttpModule } from '@angular/http';
import { CustomBrowserXhr } from './custom-browser-xhr.service';
import { BlobService } from './blob.service';
import { CommonPdfComponent } from './common-pdf/common-pdf.component';

@NgModule({
    declarations: [
        CommonPdfComponent
    ],
    providers: [
      { provide: BrowserXhr, useClass: CustomBrowserXhr },
      BlobService
    ],
    imports: [
        BrowserModule,
        HttpModule
    ],
    exports: [
        CommonPdfComponent
    ]
})
export class BlobModule { }