import { Component, OnInit } from '@angular/core';
import { BlobService } from '../blob.service';

@Component({
    moduleId: module.id,
    selector: 'common-pdf',
    templateUrl: './common-pdf.component.html',
    styleUrls: ['./common-pdf.component.css']
})
export class CommonPdfComponent implements OnInit {
    constructor(private _blobService: BlobService) { }

    ngOnInit() { }

        generateCommonPDF(): void {
        this._blobService.genCommonPDF().subscribe(
            (data) => sessionStorage.removeItem('blob'),
            (err) => sessionStorage.removeItem('blob')
        );
    }
}
