import { Injectable } from '@angular/core';
import { BrowserXhr } from '@angular/http';

@Injectable()
export class CustomBrowserXhr extends BrowserXhr {
  constructor() {
      super();
  }
  build(): any {
    let xhr = super.build();
    if (sessionStorage.getItem('blob')) {
      xhr.responseType = "blob";
    } else {
      xhr.responseType = "json";
  }
      return <any>(xhr);
  }
}