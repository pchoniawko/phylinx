import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { globalUrl } from '../common/globalUrl';
import { downloader } from '../common/downloader';
import { DocumentSettingsService } from '../common/services/document-settings.service';
import {FormattingSettingsService} from '../demo/formatting-settings/formatting-settings.service';
import {IFormattingSettings} from "../demo/formatting-settings/IFormattingSettings";
import { ContainerService } from '../common/services/container.service';
import { loadStateString } from '../common/loadState';
import { filterCharacters } from '../common/convertName';
import {Attachment} from "../common/models/attachment";

@Injectable()
export class BlobService {
    constructor(private _http: Http,
                private FormattingSettingsService:FormattingSettingsService,
                private DocumentSettingsService:DocumentSettingsService,
                private ContainerService:ContainerService,
    ) {
    }

    getRegAttNumber=():RegExp=>{
        const formattingSettings: IFormattingSettings = this.FormattingSettingsService.getFormattingSettings();
        const regNumberVar  = filterCharacters(formattingSettings.attNumber.value)+" \\d+";
        return new RegExp(regNumberVar,'g');
    };

    newServerName=(attachment:Attachment):string=>{
        const regAttNumber = this.getRegAttNumber();
        const attNumber = filterCharacters(attachment.content).match(regAttNumber).toString().trim();
        const serverName = attachment.serverName.replace(regAttNumber,"").trim();
        return filterCharacters(`${attNumber} ${serverName}`);
    };

    reducedAttachments=(attachments:Array<Attachment>):Array<Object>=>{
        return attachments
            .filter((attachment:Attachment)=>attachment.serverName!=='')
            .reduce((prev:Array<Object>,attachment:Attachment)=> {
                const newServerName = this.newServerName(attachment);
                const att ={serverName:attachment.serverName, newServerName};
                return [...prev,att];
            },[]);
    };

    updateAttachmentsServerNames(attachments:Array<Attachment>):void{
        attachments
            .filter((attachment:Attachment)=>attachment.serverName!=='')
            .map((attachment:Attachment)=>{
                attachment.serverName = this.newServerName(attachment);
                this.DocumentSettingsService.setDocumentSettings(attachment.id, attachment.toJSON());
                this.DocumentSettingsService.persistDocumentSettings();
            });
    }
    getNewServerNames = (attachments:Array<Attachment>): Object => {
        const attachmentsReduced = this.reducedAttachments(attachments);
        return  {
            attachments: attachmentsReduced,
        };
    }

    downloadFile(fileName: string): Observable<any> {

        sessionStorage.setItem('blob', 'blob');
        console.log('downloadFile', fileName);
        const headers = new Headers();
        headers.set('content-type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        const body = {fileName, documentDir:loadStateString("logged")};


        return this._http.post(globalUrl(`get-file`),body,options)
            .map((res: any) => {
                const contentType = res.headers._headers.get("content-type")[0];
                if(contentType === "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
                    const downloadName = fileName.replace('.docx',"");
                    downloader(res,'.docx',downloadName);
                }
                if(contentType === "application/pdf") {
                    const downloadName = fileName.replace('.pdf',"");
                    downloader(res,'.pdf',downloadName);
                }
            });
    }

    updateServerNames(): Observable<any> {
        const headers = new Headers();
        headers.set('content-type', 'application/json');
        let options = new RequestOptions({ headers: headers });

        const attachments = this.ContainerService.getAttachments();
        const body = {
            attachments:this.reducedAttachments(attachments),
            documentDir:loadStateString("logged")
        };
        this.updateAttachmentsServerNames(attachments);

        return this._http.post(globalUrl(`update-server-names`),body,options)
            .map((res: any) => res._body);
    }



    genCommonPDF(): Observable<any> {
        sessionStorage.setItem('blob', 'blob');
        const headers = new Headers();
        headers.set('content-type', 'application/json');
        const attachments = this.ContainerService.getAttachments();
        const body = {
            attachments:this.reducedAttachments(attachments),
            documentDir:loadStateString("logged")
        };
        this.updateAttachmentsServerNames(attachments);
        let options = new RequestOptions({ headers: headers });
        return this._http.post(globalUrl(`gen-pdf`),body,options)
            .map((res: any) => downloader(res,'.pdf'));
    }
}