import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent {
  @Input() destination: string;
  @Input() titled: string;

  constructor(private router: Router) {
  }

  back() {
    return this.router.navigate([this.destination]);
  }

}