import { Component, Input } from '@angular/core';


@Component({
  moduleId: module.id,
  selector: 'breadcrumb-dead',
  templateUrl: './breadcrumb-dead.component.html',
  styleUrls: ['./breadcrumb-dead.component.css']
})

export class BreadcrumbDeadComponent {
  @Input() destination: string;
  @Input() titled: string;

  constructor() {
  }

  

}