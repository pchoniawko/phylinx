import { Router } from '@angular/router';

import {
  Component,
  Input
} from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'command-bar',
  templateUrl: './command-bar.component.html',
  styleUrls: ['./command-bar.component.css']
})

export class CommanBarComponent {
  @Input() title: string;
  
  constructor(private router: Router) {
  }
  
  home() {
    
  }
  

}