import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { BackButtonComponent } from './back-button.component';
import { CommanBarComponent } from './command-bar.component';
import { BreadcrumbComponent } from './breadcrumb.component';
import { BreadcrumbDeadComponent } from './breadcrumb-dead.component';

@NgModule({
  imports: [
    BrowserModule
  ],
  providers: [],
  declarations: [
    BackButtonComponent,
    CommanBarComponent,
    BreadcrumbComponent,
    BreadcrumbDeadComponent
  ],
  exports: [
    BackButtonComponent,
    CommanBarComponent,
    BreadcrumbComponent,
    BreadcrumbDeadComponent
  ]
})
export class OfficeUiModule {
}