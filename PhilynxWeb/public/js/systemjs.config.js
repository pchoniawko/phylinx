(function (global) {
    var config = {
        paths: {
            'npm:': 'node_modules/'
        },
        map: {
            'addin': 'addin', // 'dist',
            // angular bundles
            '@angular/core': 'npm:@angular/core/bundles/core.umd.js',
            '@angular/common': 'npm:@angular/common/bundles/common.umd.js',
            '@angular/compiler': 'npm:@angular/compiler/bundles/compiler.umd.js',
            '@angular/platform-browser': 'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
            '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
            '@angular/http': 'npm:@angular/http/bundles/http.umd.js',
            '@angular/router': 'npm:@angular/router/bundles/router.umd.js',
            '@angular/forms': 'npm:@angular/forms/bundles/forms.umd.js',

            'angular2-cookie': 'npm:angular2-cookie',
            'rxjs': 'node_modules/rxjs',
            "ng2-dropdown": "node_modules/ng2-dropdown",
            'angular2-fontawesome': 'node_modules/angular2-fontawesome'
        },
        packages: {
            'addin': { main: 'main.js', defaultExtension: 'js' },
            'rxjs': { defaultExtension: 'js' },
            "ng2-dropdown": { "main": "index.js", "defaultExtension": "js" },
            'angular2-cookie': {
                main: './core.js',
                defaultExtension: 'js'
            },
            'angular2-fontawesome': { defaultExtension: 'js' }
        }
    };

    System.config(config);

})(this);
