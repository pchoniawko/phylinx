import * as path from 'path';
const hummus = require('hummus');


export const STAMPS_PATH = path.join(__dirname, "../", 'stamps');

const textOptions = (writer: any) => ({
			font: writer.getFontForFile(path.join(__dirname, "../", '/fonts/OpenSans-BoldItalic.ttf')),
			size: 14,
			colorspace: 'gray',
			color: 0x00,
			underline: true
		});



export const addStamps = async (inputPath: string, documentDir: string) => {
    const re = /[\/|\\]/g;
    const fileName = inputPath.split(re).reverse()[0];
    const input = path.join( documentDir, inputPath);
    console.log(input);
    const stampPath = path.join( documentDir, 'stamped');


    const outputName = path.join( documentDir, 'stamped', fileName);

    console.log({fileName, inputPath, outputName, input });
    const writer = hummus.createWriterToModify(input, {
      modifiedFilePath: outputName
    });
    const pdfReader = hummus.createReader(input);

    const newPage = writer.createPage(0,0,595,842);
    const cxt = writer.startPageContentContext(newPage);

    for (let i=0; i < pdfReader.getPagesCount(); ++i) {
        const pageModifier = new hummus.PDFPageModifier(writer, i, true);
        const pageTop = writer.getModifiedFileParser().parsePage(i).getMediaBox()[3];
        const imageHeight = writer.getImageDimensions(path.join(STAMPS_PATH, 'jpeg.jpg')).height;

        pageModifier.startContext().getContext().drawImage(i, pageTop-imageHeight, path.join(STAMPS_PATH, 'jpeg.jpg'));
        pageModifier.endContext().writePage();
    }
    writer.writePage(newPage);
    writer.end();

    return outputName;
};
