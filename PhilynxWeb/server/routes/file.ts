import * as express from "express";
import * as path from "path";
import * as fs from "fs";
import {convert2PDF,deleteDirectory,updateServerFilesNames, copyAllFilesToPackages} from "./utils";

const glob = require("glob");

const merge = require("../../Scripts/PDFMerger.js");
import { addStamps, STAMPS_PATH } from '../services/stamps.service';

export class File {
    public static routes = (): express.Router => {
        const router: express.Router = express.Router();
        const fileRoute: File = new File();
        router.post("/save-file", fileRoute.saveAttachment);
        router.post("/get-file", fileRoute.getFile);
        router.post("/delete-file", fileRoute.deleteFile);
        router.post("/gen-pdf", fileRoute.genMergedPDF);
        router.post("/update-server-names", fileRoute.updateServerNames);
        router.post("/stamp-upload", fileRoute.stampUpload);
        router.post("/pack-files", fileRoute.packFiles);

        return router;
    }

    public saveAttachment = (req: express.Request, res: express.Response): void => {
        try {
            const {documentDir, file, name, packageSize} = req.body;
            console.log(documentDir, name, packageSize);

            let type = file.split(';')[0].split('/')[1];
            if (type === 'vnd.openxmlformats-officedocument.wordprocessingml.document') {
                type = 'docx';
            }
            const base64Data = file
                .replace('data:application/pdf;base64,', "")
                .replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,', "");
            const regExp = /[éèêëùüàâöïç\"\/.'?!,@$#_]/g;
            console.log("name", name);
            const fileName = name
                .replace(regExp, "")
                .trim();
            const uploadPath = path.join(documentDir, `${fileName}.${type}`);
            fs.writeFile(uploadPath, base64Data, {encoding: 'base64'}, (err) => {
                if (type === "docx") {
                    //convert2PDF(documentDir, fileName);
                }
                res.status(201).json({
                    success: true,
                    massage: "File saved",
                    fileName: `${fileName}.${type}`
                });
            });

        } catch (err) {
            res.status(400).json({
                success: false,
                massage: err.message
            });
        }
    }

    public getFile = (req: express.Request, res: express.Response) => {
        try {
            const {fileName, documentDir} = req.body;
            console.log('getFile', req.body);
            res.setHeader('Content-Type', "application/pdf");
            res.setHeader("filename", fileName);
            res.sendFile(path.join(documentDir, fileName));
        } catch (err) {
            res.status(400).json({
                success: false,
                massage: err.message
            });
        }
    }


    public updateServerNames = (req: express.Request, res: express.Response) => {
        try {
            const {documentDir,attachments} = req.body;
            console.log("body",req.body);
            updateServerFilesNames(attachments,documentDir);
            res.status(200).json({
                success: true,
                massage: "Server names updated"
            });

        } catch (err) {
            res.status(400).json({
                success: false,
                massage: err.message
            });
        }
    }

    public packFiles= (req: express.Request, res: express.Response) => {
        try {
            const {documentDir,packageSize} = req.body;
            console.log("body",req.body);
            copyAllFilesToPackages(documentDir,packageSize);
            res.status(200).json({
                success: true,
                massage: 'Files packed'
            });


        } catch (err) {
            res.status(400).json({
                success: false,
                massage: err.message
            });
        }
    }

    public genMergedPDF = (req: express.Request, res: express.Response) => {
            const {documentDir,attachments} = req.body;
            console.log("body ",req.body);
            return new Promise((resolve, reject) => {
                fs.readdir(documentDir, async (err, files) => {
                    if( err ) {
                        console.error( "Could not list the directory.", err );
                        process.exit( 1 );
                        reject(err);
                    }
                    const stampPath = path.join( documentDir, 'stamped');

                    if(fs.existsSync(stampPath)){
                        await deleteDirectory(stampPath);
                        fs.mkdirSync(stampPath);

                    } else {
                        fs.mkdirSync(stampPath);
                    }
                    files.forEach((file, index) => {
                        console.log({file});
                        if (file.includes('.pdf')) {
                            addStamps(file, documentDir);
                        }
                    });
                    resolve();
                });
            }).then(() => {
               updateServerFilesNames(attachments,documentDir);
                console.log(`${documentDir}/stamped/*.pdf`, 'merged');
                glob(`${documentDir}/stamped/*.pdf`, {nodir: true}, (err: any, files: any) => {
                    if (files.length > 1) {
                        merge(files, `${documentDir}/merged.pdf`, (err: any) => {
                            if (err) {
                                console.log(err);
                            }
                            console.log('Success');
                            res.setHeader('Content-Type', "application/pdf");
                            res.setHeader("filename", "merged");
                            res.sendFile(path.join(documentDir, 'merged.pdf'), () => {
                                fs.unlink(path.join(documentDir, 'merged.pdf'), (err: any) => {
                                    if (err) {
                                        console.log(err);
                                    }
                                });
                            });
                        });
                    } else {
                        res.setHeader('Content-Type', "application/pdf");
                        res.setHeader("filename", "merged");
                        res.sendFile(path.join(documentDir, 'merged.pdf'), () => {
                            fs.unlink(path.join(documentDir, 'merged.pdf'), (err: any) => {
                                if (err) {
                                    console.log(err);
                                }
                            });
                        });
                    };
            });

        }); 
    }

    public deleteFile = (req: express.Request, res: express.Response) => {
        try {
            const {fileName, documentDir} = req.body;
            const filePath = path.join(documentDir, fileName);
            console.log("filepath", filePath);
            fs.unlink(filePath, (err: any) => {
                if (err) {
                    console.log(err);
                }
                console.log("usunieto");

                res.json({
                    success: true,
                    message: 'File deleted.'
                });
            });
        } catch (err) {
            res.status(400).json({
                success: false,
                massage: err.message
            });
        }
    }
    public stampUpload = (req: express.Request, res: express.Response) => {
        const { fileName, file } = req.body;
        const fileData = file.replace(/^data:image\/jpeg;base64,/, "");
        fs.writeFile(`${STAMPS_PATH}/${fileName}`, fileData, {encoding: 'base64'}, (err) => {
            res.status(201).json({
                success: true,
                massage: "File saved",
                fileName: fileName
            });
        });

    }
}