import * as express from "express";
import * as fs from "fs";
import {createDirectory} from "./utils";


export class Auth {
    public static routes = (): express.Router => {
        const router: express.Router = express.Router();
        const authRoute: Auth = new Auth();
        router.post("/create-document", authRoute.createDocument);
        return router;
    }

    public createDocument = (req: express.Request, res: express.Response): void => {
        try {
            const {documentDir, packageSize} = req.body;

            if (!fs.existsSync(documentDir)) {
                createDirectory(documentDir);
            }

            res.status(200).json({
                success: true,
                message: `Directory ${documentDir} created!`,
                documentDir: documentDir,
                packageSize: packageSize
            });

        } catch (err) {
            res.status(400).json({
                success: false,
                massage: err.message
            });
        }
    }

}