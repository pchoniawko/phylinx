import * as express from "express";
import * as fs from "fs";
import * as path from "path";
import {Attachment} from "../../addin/common/models/attachment";
const glob = require("glob");
const rimraf = require("rimraf");
// const msopdf = require('node-msoffice-pdf');

export const mimeTypes = {
    openxmlformats: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    pdf: "application/pdf"
};

export const convert2PDF = (dirPath: string,fileName:string): Promise<string> => {
    return new Promise((resolve) => {
        // msopdf(null, function (error: any, office: any) {
        //     if (error) {
        //         console.log("Init failed", error);
        //         return;
        //     }
        //     let filePath: string;
        //     console.log("filePath",`${dirPath}/${fileName}.docx`);
        //     office.word({input:`${dirPath}/${fileName}.docx`, output: `${dirPath}/${fileName}.pdf`}, (error: any, pdf: any) => {
        //         if (error) {
        //             console.log("Woops", error);
        //         } else {
        //             filePath = pdf;
        //             console.log("Saved to", pdf);
        //         }
        //     });
        //     office.close(null, (error: any) => {
        //         if (error) {
        //             console.log("Woops", error);
        //         } else {
        //             console.log("Finished & closed");
        //             resolve(filePath);
        //         }
        //     });

        // });
    });

}

export const updateServerFilesNames = (attachments:Array<Attachment>, fullDirectoryPath:string) => {
    attachments.map((attachment:any)=>{
        console.log(path.join(fullDirectoryPath,attachment.serverName), path.join(fullDirectoryPath,`${attachment.newServerName}`));
        fs.renameSync(path.join(fullDirectoryPath,attachment.serverName), path.join(fullDirectoryPath,`${attachment.newServerName}`));
    });
};


export const saveToPackage = async (file: any, directoryPath: string, packageSize: any, fileName: string) => {
    let iterator = 0;
    let saved: any = false;
    while (!saved) {
        let uploadPath = path.join(directoryPath, `/${packageSize}MB/attachments-${iterator}`);
        if (!fs.existsSync(uploadPath)) {
            fs.mkdirSync(uploadPath);
            saved = await this.saveFileToDirectory(uploadPath, file, fileName);
        } else {
            const enoughSpace = await this.allowedToSave(uploadPath, file, packageSize);
            if (enoughSpace) {
                saved = await this.saveFileToDirectory(uploadPath, file, fileName);
            } else {
                iterator++;
            }
        }
    }
}

export const getFilesizeInBytes = (filepath: string) => {
    const stats = fs.statSync(filepath);
    return stats.size / 1000000.0;
}

export const deleteDuplicateFromDirectory = (directoryPath: string, file: any) => {
    return new Promise((resolve, reject) => {
        glob(`${directoryPath}/*`, {nodir: true}, async (err: any, filesPaths: any) => {
            for (const filePath of filesPaths) {
                await this.deleteDuplicateFile(filePath, file);
            }
            resolve();
        });
    });
}

export const checkDirectorySize = (directoryPath: string, file: any) => {
    return new Promise((resolve, reject) => {
        let dirSize = 0;
        glob(`${directoryPath}/*`, {nodir: true}, (err: any, filesPaths: any) => {
            for (const filePath of filesPaths) {
                dirSize += this.getFilesizeInBytes(filePath);
            }
            resolve(dirSize);
        });
    });
}

export const convertFileToBase64=(filPath:string)=>{
    return new Promise((resolve, reject) => {
        fs.readFile(filPath, (err, data) => {
            if (err) {
                console.log(err);
            }
            const file = new Buffer(data).toString('base64');
            resolve(file);
        });
    });
}

export const deleteDuplicateFile = (firstFilePath: any, secondFile: any) => {
    return new Promise((resolve, reject) => {
        fs.readFile(firstFilePath, (err, data) => {
            if (err) {
                console.log(err);
            }
            const firstFile = new Buffer(data).toString('base64');
            if (firstFile.slice(-1000) === secondFile.slice(-1001, -1)) {
                fs.unlink(firstFilePath, (err: any) => {
                    if (err) {
                        console.log(err);
                    }
                    resolve();
                });
            } else {
                resolve();
            }
        });
    });
};

export const allowedToSave = (directoryPath: string, file: string, packageSize: any) => {
    return new Promise((resolve, reject) => {
        let size = 0;
        const fileSize = Buffer.byteLength(file, 'base-64') / 1000000.0;
        // this.deleteDuplicateFromDirectory(directoryPath, file).then(() => {
        this.checkDirectorySize(directoryPath, file).then((dirSize: any) => {
            size = fileSize + dirSize;
            size < packageSize ? resolve(true) : resolve(false);
        });
    });
    // });
}

export const saveFileToDirectory = (uploadPath: string, file: any, fileName: string) => {
    return new Promise((resolve, reject) => {
        const base64Data = file
            .replace('data:application/pdf;base64,', "")
            .replace('data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,', "");
        fs.writeFile(`${uploadPath}/${fileName}`, base64Data, {encoding: 'base64'}, (err) => {
            if (err) {
                console.log(err);
            }
            resolve(true);
        });
    });
}

export const copyAllFilesToPackages = (directoryPath: string, packageSize: any) => {
    const packagePath=path.join(directoryPath, `${packageSize}MB`);
    rimraf(packagePath, (err: any) => {
        if (err) {
            throw err;
        }
        fs.mkdirSync(packagePath);
        glob(`${directoryPath}/*.pdf`, {nodir: true}, async (err: any, files: any) => {
            for (const filePath of files) {
                await this.copyFileToDirectory(filePath, directoryPath, packageSize);
            }
        });
    });

}

export const copyFileToDirectory = (filePath: string, directoryPath: string, packageSize: any) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, (err, data) => {
            if (err) {
                console.log(err);
            }
            const fileName = path.basename(filePath);
            const file = new Buffer(data).toString('base64');
            this.saveToPackage(file, directoryPath, packageSize, fileName).then((result: any) => {
                resolve();
            });
        });
    });
}

export const deleteSubDirectoriesExceptChosen = (directoryPath: string, subFolderName: string) => {
    const subDirectories = this.getDirectories(directoryPath);
    subDirectories
        .filter((directory: any) => directory !== subFolderName)
        .map((directory: any) => {
            rimraf(path.join(directoryPath, `${directory}`), (err: any) => {
                if (err) {
                    throw err;
                }
            });
        });
}

export const createDirectory = (dirPath: string) => {
    return new Promise((resolve, reject) => {
        const directoryPath = path.join(dirPath);
        fs.mkdir(directoryPath, (err:any) => {
            if (err) {
                throw err;
            }
            resolve();
        });
    });
};
export const deleteDirectory=(path:string)=>{
    return new Promise((resolve,reject)=>{
        rimraf(path, (err: any) => {
            if (err) {
                throw err;
            }
            resolve();
        });
    });
}

export const getDirectories = (srcpath: string) => {
    return fs.readdirSync(srcpath)
        .filter(file => fs.lstatSync(path.join(srcpath, file)).isDirectory());
};
