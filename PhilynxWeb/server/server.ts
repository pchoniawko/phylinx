import * as express from "express";
import * as https from "https";
import * as path from "path";
import * as fs from "fs";
import * as bodyParser from "body-parser";
import * as FileRoute from "./routes/file";
import * as AuthRoute from "./routes/auth";

const PORT = 4000;

export class Server {
    private app: express.Express;
    private server: any;
    private options = {
        key: fs.readFileSync(path.join(__dirname, "cert", "localhost.key")),
        cert: fs.readFileSync(path.join(__dirname, "cert", "localhost.cert")),
    };

    constructor() {
        this.app = express();
        this.setHeaders();
        this.app.use(bodyParser.json({limit: '50mb'}));
        this.app.use(bodyParser.urlencoded( {extended: true}));
        this.server = https.createServer(this.options,this.app);
        this.setRoutes();
        this.setStaticRoutes();
     }

     public bootstrap(): Server {
         return new Server();
     }

     public setHeaders(): void {
        this.app.use((req, res, next) => {
          res.header("Access-Control-Allow-Origin", "*");
          res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, enctype");
          next();
        });
     }

     public setRoutes(): void {
         const router: express.Router = express.Router();
         router.use(FileRoute.File.routes());
         router.use(AuthRoute.Auth.routes());
         this.app.use(router);
     }

     private setStaticRoutes() {
        this.app.use("/node_modules", express.static(
            path.join(__dirname, "../../node_modules")
        ));
        this.app.set("views", path.join(__dirname, "../client"));
        this.app.use(express.static(path.join(__dirname, "../client")));
        this.app.engine(".html", require("ejs").__express);
        this.app.set("view engine", "html");

    }

     public startServer(): void {
         this.server.listen(PORT, () => {
             console.log(`Aplication listening on ${PORT}`);
         });
     }
}